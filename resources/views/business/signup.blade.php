@include("business.layouts.common")

<div class="form-wrapper">
  <div class="card-header bg-danger text-white text-center"><h1>会員登録</h1></div>
    <div class="card-body p-4">
    <form class="simple_form new_registration" id="new_registration" action="/signup/confirm" accept-charset="UTF-8" method="post">
      <div class="form-group row string required">
        <label class="col-sm-3 col-form-label string required">会社名</label>
        <div class="col-sm-9">
          <input class="form-control string required" required="required" aria-required="true" type="text" name="company_name">
        </div>
      </div>
      <div class="form-group row string required">
        <label class="col-sm-3 col-form-label string required">お名前</label>
        <div class="col-sm-9">
          <input class="form-control string required" required="required" aria-required="true" type="text" name="name">
        </div>
      </div>
      <div class="form-group row tel required">
        <label class="col-sm-3 col-form-label tel required">電話番号</label>
        <div class="col-sm-9">
          <input class="form-control string tel required" required="required" aria-required="true" type="tel" name="phone_number">
        </div>
      </div>
      <div class="form-group row email required">
        <label class="col-sm-3 col-form-label email required">メールアドレス</label>
        <div class="col-sm-9">
          <input class="form-control string email required" required="required" aria-required="true" placeholder="" type="email" name="email">
        </div>
      </div>
      <div class="form-group row password required">
        <label class="col-sm-3 col-form-label password required">パスワード</label>
        <div class="col-sm-9">
          <input class="form-control password required" required="required" aria-required="true" type="password" name="password">
          <small class="form-text text-muted">半角英数8文字以上。英字、数字をそれぞれ1文字以上含めてください。</small>
        </div>
      </div>
      <div class="form-group row password required">
        <label class="col-sm-3 col-form-label password_confirm required">パスワード確認</label>
        <div class="col-sm-9">
          <input class="form-control password required" required="required" aria-required="true" type="password" name="password_confirm">
        </div>
      </div>
      <hr class="my-4">
      <div class="text-center">
        <div class="form-check mb-4">
          <input id="confirmCheck" class="form-check-input" type="checkbox"><label for="confirmCheck" class="form-check-label">上記に同意する</label>
        </div>
        <input type="submit" name="commit" value="入力内容の確認" id="confirmButton" class="btn btn-lg btn-success disabled" data-disable-with="入力内容の確認">
      </div>
    </form>
  </div>
</div>