@include("business.layouts.common")
@include("business.layouts.header")

  <!-- Page Content -->
  <div class="container">

    <h1 class="my-4">非会員ページまだ</h1>

    <div class="row">
      <div class="col-lg-6 portfolio-item">
        <div class="card h-100">
          <a href="/sell"><img class="card-img-top" src="http://placehold.it/468x120?text=Sell" alt=""></a>
        </div>
      </div>
      <div class="col-lg-6 portfolio-item">
        <div class="card h-100">
          <a href="/buy"><img class="card-img-top" src="http://placehold.it/468x120?text=Buy" alt=""></a>
        </div>
      </div>
    </div>

    <h2 class="my-4">物件紹介</h2>

    <div class="row">
      <div class="col-md-5">
        <a href="#">
          <img class="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/400x300?text=Estate Image" alt="">
        </a>
      </div>
      <div class="col-md-7">
        <h3>セカンダリー蒲生</h3>
        <p>鹿児島県姶良市蒲生町上久徳2259-1</p>
        <table class="table">
          <tr>
              <th class="table-active">物件価格</th>
              <td>5,000,000,000円</td>
              <th class="table-active">表面利回り</th>
              <td>13.58%</td>
          </tr>
          <tr>
              <th class="table-active">売電単価</th>
              <td>40円</td>
              <th class="table-active">システム容量</th>
              <td>2000.58kw</td>
          </tr>
        </table>
        <a class="btn btn-success" href="{{ route('contact.buy.input') }}">問い合わせ
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-5">
        <a href="#">
          <img class="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/400x300?text=Estate Image" alt="">
        </a>
      </div>
      <div class="col-md-7">
        <h3>セカンダリー蒲生</h3>
        <p>鹿児島県姶良市蒲生町上久徳2259-1</p>
        <table class="table">
          <tr>
              <th class="table-active">物件価格</th>
              <td>5,000,000,000円</td>
              <th class="table-active">表面利回り</th>
              <td>13.58%</td>
          </tr>
          <tr>
              <th class="table-active">売電単価</th>
              <td>40円</td>
              <th class="table-active">システム容量</th>
              <td>2000.58kw</td>
          </tr>
        </table>
        <a class="btn btn-success" href="{{ route('contact.buy.input') }}">問い合わせ
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
    </div>
    <hr>

    <h2 class="my-4">お客様の声</h2>

    <div class="row">
      <div class="col-md-4">
        <a href="#">
          <img class="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/400x200?text=Customer Image" alt="">
        </a>
      </div>
      <div class="col-md-8">
        <h3>兵庫県 T・M様 男性</h3>
        <p>サラリーマンの節税目的でアパート経営をしておりましたが収益が良いため新たな経費を作るつもりで太陽光発電事業を知り今期から方針を変えてみました。</p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
        <a href="#">
          <img class="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/400x200?text=Customer Image" alt="">
        </a>
      </div>
      <div class="col-md-8">
        <h3>神奈川県 S・H様 男性</h3>
        <p>サラリーマンの節税目的でアパート経営をしておりましたが収益が良いため新たな経費を作るつもりで太陽光発電事業を知り今期から方針を変えてみました。</p>
      </div>
    </div>
    <hr>

    <h2 class="my-4">メディア掲載実績</h2>

    <div class="row">
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Media" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Media" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Media" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Media" alt="">
      </div>
    </div>
    <div class="row">
    <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Media" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Media" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Media" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Media" alt="">
      </div>
    </div>

    <div class="card mb-4">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-6">
              <img class="img-fluid rounded m-2" src="http://placehold.it/700x300" alt="">
          </div>
          <div class="col-lg-6">
            <h2 class="card-title">会員様には物件情報をご紹介します</h2>
            <p class="card-text">会員登録をして頂くと、物件情報の閲覧・検索ができます</p>
            <a class="btn btn-danger btn-lg m-2" href="{{ route('register') }}">会員登録</a>
            <a class="btn btn-success btn-lg m-2" href="{{ route('contact.other.input') }}">問い合わせ</a>
          </div>
        </div>
      </div>
    </div>

    <h2>新着ニュース</h2>

    <div class="m-4">
      <ul class="list-group list-group-flush col-lg-12">
        <li class="list-group-item"><span class="mx-4">2020/06/29</span><a href="#">新型コロナウイルスの感染拡大防止に伴うお問い合わせ方法変更について</a></li>
        <li class="list-group-item"><span class="mx-4">2020/05/19</span><a href="#">物件入荷しました！！</a></li>
        <li class="list-group-item"><span class="mx-4">2020/04/09</span><a href="#">物件入荷しました！！</a></li>
        <li class="list-group-item text-right"><a href="#">もっと見る</a></li>
      </ul>
    </div>

    <div class="row">
      <div class="col-lg-6 mb-4">
        <h2>太陽光私設取引について</h2>
        <p>LFIT期間が終了したあとは売電ができなくなるのではないかと不安な人も多いのではないでしょうか。
          この記事では、「卒FITとはどういうことなのか」「新電力への売電はメリットがあるのか」について解説します。
          この記事を読むことによって、FIT期間が終わったあとの不安もなくなり、余剰電力をお得に活用する方法がわかります。</p>
      </div>
      <div class="col-lg-6 mb-4">
        <img class="img-fluid rounded" src="http://placehold.it/700x300" alt="">
      </div>
    </div>
    <!-- /.row -->

    <h2>おすすめ情報</h2>

    <div class="row">
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Info" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Info" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Info" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Info" alt="">
      </div>
    </div>

@include("business.layouts.footer")