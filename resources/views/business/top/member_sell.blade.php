@include("business.layouts.common")
@include("business.layouts.navi")

  <!-- Page Content -->
  <div class="container">

    <h1 class="my-4">会員専用ページ</h1>

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{ route('top.index') }}">TOP</a>
      </li>
      <li class="breadcrumb-item active">売却・転売</li>
    </ol>

    <img class="img-fluid rounded mb-4" src="http://placehold.it/1200x450?text=Sell Message" alt="">
    <img class="img-fluid rounded mb-4" src="http://placehold.it/1200x200?text=Flow" alt="">

    <div class="text-center mb-4">
      <a class="btn btn-success btn-lg m-2" href="{{ route('contact.buy.input') }}">問い合わせ</a>
    </div>

@include("business.layouts.footer")