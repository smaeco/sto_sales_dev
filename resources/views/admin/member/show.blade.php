@include("admin.layouts.header")
@include("admin.layouts.navi")
@include("admin.layouts.sidebar")

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">会員詳細</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}">TOP</a></li>
              <li class="breadcrumb-item active">会員詳細</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8">
          @include("admin.layouts.message")
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                  </thead>
                  <tbody>
                    <tr>
                      <th style="width:200px;">ID</th>
                      <td>{{ $member->id }}</td>
                    </tr>
                    <tr>
                      <th>種別</th>
                      <td>{{ config('const.MEMBER.CUSTOMER_TYPE')[$member->customer_type] }}</td>
                    </tr>
                    <tr>
                      <th>会員種別</th>
                      <td>{{ config('const.MEMBER.MEMBER_STATUS')[$member->member_status] }}</td>
                    </tr>
                    <tr>
                      <th>企業名</th>
                      <td>{{ $member->company_name }}</td>
                    </tr>
                    <tr>
                      <th>個人名</th>
                      <td>{{ $member->personal_name }}</td>
                    </tr>
                    <tr>
                      <th>電話番号</th>
                      <td>{{ $member->tel }}</td>
                    </tr>
                    <tr>
                      <th>メールアドレス</th>
                      <td>{{ $member->email }}</td>
                    </tr>
                    <tr>
                      <th>住所</th>
                      <td>{{ $member->address_1 }}{{ $member->address_2 }}{{ $member->address_3 }}</td>
                    </tr>
                    <tr>
                      <th>削除日時</th>
                      <td>{{ $member->deleted_at ? $member->deleted_at->format('Y/m/d h:i:s') : "" }}</td>
                    </tr>
                    <tr>
                      <th>作成日時</th>
                      <td>{{ $member->created_at->format('Y/m/d h:i:s') }}</td>
                    </tr>
                    <tr>
                      <th>更新日時</th>
                      <td>{{ $member->updated_at->format('Y/m/d h:i:s') }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-4">
            <div class="row m-2">
              <a href="{{ route('admin.member.edit', ['memberId' => $member->id])}}" type="button" class="btn btn-success m-1 w-25 btn-flat">編集</a>
            </div>
          </div>
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
  </div>

@include("admin.layouts.footer")
