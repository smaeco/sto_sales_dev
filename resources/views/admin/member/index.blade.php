@include("admin.layouts.header")
@include("admin.layouts.navi")
@include("admin.layouts.sidebar")

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">会員一覧</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.home.index')}}">TOP</a></li>
              <li class="breadcrumb-item active">会員一覧</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">

              <!-- 検索フォーム -->
              <div class="card-header">
                <form name="member_search" id="memberSearch" action="{{ route('admin.member.index')}}" method="get">
                  <div class="row">
                    <div class="col-sm-6">
                      <label>企業名</label>
                      <div class="form-group input-group input-group-sm col-10">
                        <input type="text" class="form-control" name="company_name" value="{{ $params['company_name'] }}">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <label>会員名</label>
                      <div class="form-group input-group input-group-sm col-10">
                        <input type="text" class="form-control" name="personal_name" value="{{ $params['personal_name'] }}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-3">
                      <label>電話番号</label>
                      <div class="form-group input-group input-group-sm col-12">
                        <input type="text" class="form-control" name="tel" value="{{ $params['tel'] }}">
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <label>メールアドレス</label>
                      <div class="form-group input-group input-group-sm col-12">
                        <input type="text" class="form-control" name="email" value="{{ $params['email'] }}">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <label>会員種別</label>
                      <div class="form-group">
                        @foreach(config('const.MEMBER.MEMBER_STATUS') as $key => $value)
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input class="custom-control-input" type="checkbox" id="memberStatusCheckbox{{ $key }}" name="member_status[]" value="{{ $key }}" {{is_array($params['member_status']) ? in_array($key, $params['member_status']) ? ' checked' : '' : ' checked'}}>
                          <label for="memberStatusCheckbox{{ $key }}" class="custom-control-label">{{ $value }}</label>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <label>種別</label>
                      <div class="form-group">
                        @foreach(config('const.MEMBER.CUSTOMER_TYPE') as $key => $value)
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input class="custom-control-input" type="checkbox" id="customerTypeCheckbox{{ $key }}" name="customer_type[]" value="{{ $key }}" {{is_array($params['customer_type']) ? in_array($key, $params['customer_type']) ? ' checked' : '' : ' checked'}}>
                          <label for="customerTypeCheckbox{{ $key }}" class="custom-control-label">{{ $value }}</label>
                        </div>
                        @endforeach
                      </div>
                    </div>
                    <div class="col-sm-6 d-flex align-items-center">
                      <button type="submit" class="btn btn-primary btn-flat">検索</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- //検索フォーム -->

              <div class="card-body">
                <div class="row">
                  <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr role="row">
                          <th>ID</th>
                          <th>企業名</th>
                          <th>会員名</th>
                          <th>電話番号</th>
                          <th>メールアドレス</th>
                          <th>会員種別</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($members as $member)
                        <tr role="row">
                          <td>{{ $member->id }}</td>
                          <td>{{ $member->company_name }}
                            @if($member->customer_type==1)
                            <span class="badge bg-primary">企業</span>
                            @elseif($member->customer_type==2)
                            <span class="badge bg-success">機関投資家</span>
                            @endif                          
                          </td>
                          <td>{{ $member->personal_name }}</td>
                          <td>{{ $member->tel }}</td>
                          <td>{{ $member->email }}</td>
                          <td>{{ config('const.MEMBER.MEMBER_STATUS')[$member->member_status] }}</td>
                          <td>
                            <a href="{{ route('admin.member.show', ['memberId' => $member->id])}}" type="button" class="btn btn-block btn-info btn-flat btn-sm">詳細</a>
                            <a href="{{ route('admin.member.edit', ['memberId' => $member->id])}}" type="button" class="btn btn-block btn-warning btn-flat btn-sm">編集</a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                      <tr>
                        <th>ID</th>
                        <th>企業名</th>
                        <th>会員名</th>
                        <th>電話番号</th>
                        <th>メールアドレス</th>
                        <th>種別</th>
                        <th>会員種別</th>
                      </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                      <ul class="pagination justify-content-center">
                      {{ $members->appends($params)->links() }}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
  </div>

@include("admin.layouts.footer")
