<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Estate;
use Carbon\Carbon;

class EstateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estateQuery = Estate::query()
        // 物件名
        ->when(request('estate_name'), function ($query) {
            return $query->where('estate_name', 'like', '%'.request('estate_name').'%');
        })
        // 都道府県
        ->when(request('prefecture_id'), function ($query) {
            return $query->where('prefecture_id', request('prefecture_id'));
        })
        // 電圧
        ->when(request('voltage_type'), function ($query) {
            return $query->where('voltage_type', request('voltage_type'));
        })
        // 価格下限
        ->when(request('price_lower'), function ($query) {
            return $query->where('price', '>=', request('price_lower') * 10000);
        })
        // 価格上限
        ->when(request('price_upper'), function ($query) {
            return $query->where('price', '<=', request('price_upper') * 10000);
        })
        // 表面利回り下限
        ->when(request('coupon_yield_lower'), function ($query) {
            return $query->where('coupon_yield', '>=', request('coupon_yield_lower'));
        })
        // 表面利回り上限
        ->when(request('coupon_yield_upper'), function ($query) {
            return $query->where('coupon_yield', '<=', request('coupon_yield_upper'));
        })
        // キーワード
        ->when(request('keyword'), function ($query) {
            return $query->where('keyword', 'like', '%'.request('keyword').'%');
        });
        $estates = $estateQuery->paginate(10);
        $params = [
            'prefecture_id'              => request('prefecture_id'),
            'estate_name'                => request('estate_name'),
            'voltage_type'               => request('voltage_type'),
            'price_lower'                => request('price_lower'),
            'price_upper'                => request('price_upper'),
            'coupon_yield_lower'         => request('coupon_yield_lower'),
            'coupon_yield_upper'         => request('coupon_yield_upper'),
            'interconnection_date_start' => request('interconnection_date_start'),
            'interconnection_date_end'   => request('interconnection_date_end'),
            'keyword'                    => request('keyword'),
            'sales_status'               => request('sales_status')
        ];;

        return view('admin.estates.index', compact('estates', 'params'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.estates.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'estate_name'                 => 'required',
            'location'                    => 'required',
            'prefecture_id'               => 'required',
            'price'                       => 'required|numeric',
            'coupon_yield'                => 'required|numeric',
            'fit_unit_price'              => 'required|numeric',
            'maximum_output'              => 'required|numeric',
            'interconnection_date'        => 'required|date_format:Y-m',
            'land_contract_type'          => 'required',
            'irradiation'                 => 'required',
            'area'                        => 'required',
            'voltage_type'                => 'required',
            'annual_energy_generation'    => 'required',
            'annual_energy_income'        => 'required',
            'operation_date'              => 'required|date_format:Y-m',
            'sell_type'                   => 'required',
            'output_suppression_flg'      => 'required',
            'auth_status'                 => 'required',
            'epc_free_flg'                => 'required',
            'recommend_flg'               => 'required',
            'module_maker'                => 'required',
            'module_configure'            => 'required',
            'power_conditioner_maker'     => 'required',
            'power_conditioner_configure' => 'required',
            'total_system_output'         => 'required',
            //'land_price'                  => 'required',
            //'insurance'                   => 'required',
            //'monitoring'                  => 'required',
            //'maintenance'                 => 'required',
            //'memo_1'                      => 'required',
            //'memo_2'                      => 'required',
            //'memo_3'                      => 'required',
            //'memo_4'                      => 'required',
            //'memo_5'                      => 'required',
        ]);
        $estate = new Estate([
            'estate_name'                 => $request->input('estate_name'),
            'location'                    => $request->input('location'),
            'prefecture_id'               => $request->input('prefecture_id'),
            'price'                       => $request->input('price'),
            'coupon_yield'                => $request->input('coupon_yield'),
            'fit_unit_price'              => $request->input('fit_unit_price'),
            'maximum_output'              => $request->input('maximum_output'),
            'interconnection_date'        => $request->input('interconnection_date'),
            'land_contract_type'          => $request->input('land_contract_type'),
            'irradiation'                 => $request->input('irradiation'),
            'area'                        => $request->input('area'),
            'voltage_type'                => $request->input('voltage_type'),
            'annual_energy_generation'    => $request->input('annual_energy_generation'),
            'annual_energy_income'        => $request->input('annual_energy_income'),
            'operation_date'              => $request->input('operation_date'),
            'sell_type'                   => $request->input('sell_type'),
            'output_suppression_flg'      => $request->input('output_suppression_flg'),
            'auth_status'                 => $request->input('auth_status'),
            'epc_free_flg'                => $request->input('epc_free_flg'),
            'recommend_flg'               => $request->input('recommend_flg'),
            'module_maker'                => $request->input('module_maker'),
            'module_configure'            => $request->input('module_configure'),
            'power_conditioner_maker'     => $request->input('power_conditioner_maker'),
            'power_conditioner_configure' => $request->input('power_conditioner_configure'),
            'total_system_output'         => $request->input('total_system_output'),
            'land_price'                  => $request->input('land_price'),
            'insurance'                   => $request->input('insurance'),
            'monitoring'                  => $request->input('monitoring'),
            'maintenance'                 => $request->input('maintenance'),
            'memo_1'                      => $request->input('memo_1'),
            'memo_2'                      => $request->input('memo_2'),
            'memo_3'                      => $request->input('memo_3'),
            'memo_4'                      => $request->input('memo_4'),
            'memo_5'                      => $request->input('memo_5'),
        ]);

        if ($estate->save()) {
            $request->session()->flash('success', '物件情報を追加しました');
            return redirect()->route('admin.estate.show', ['estateId' => $estate->id]);
        } else {
            $request->session()->flash('error', '追加に失敗しました');
            return redirect()->route('admin.estate.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $estate = Estate::find($id);
        return view('admin.estates.show', compact('estate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);
        return view('admin.member.edit', compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'member_status' => 'required',
            'customer_type' => 'required',
            'company_name'  => 'required',
            'personal_name' => 'required',
            'tel'           => 'required',
            'email'         => 'required|email',
        ]);
        $member = Member::findOrFail($id);
        $member->member_status = $request->input('member_status');
        $member->customer_type = $request->input('customer_type');
        $member->company_name  = $request->input('company_name');
        $member->personal_name = $request->input('personal_name');
        $member->tel           = $request->input('tel');
        $member->email         = $request->input('email');
        $member->address_1     = $request->input('address_1');
        $member->address_2     = $request->input('address_2');
        $member->address_3     = $request->input('address_3');

        if ($member->save()) {
            $request->session()->flash('success', '会員情報を更新しました');
        } else {
            $request->session()->flash('error', '更新に失敗しました');
        }
        return redirect()->route('admin.member.show', ['memberId' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
