<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('estate_name', 100);
            $table->string('location', 255);
            $table->unsignedInteger('prefecture_id');
            $table->float('irradiation', 5, 2)->nullable();
            $table->float('area', 8, 2)->nullable();
            $table->unsignedBigInteger('price');
            $table->unsignedTinyInteger('voltage_type');
            $table->unsignedTinyInteger('land_contract_type');
            $table->float('annual_energy_generation', 10, 2);
            $table->unsignedBigInteger('annual_energy_income')->nullable();
            $table->float('fit_unit_price', 4, 1);
            $table->date('interconnection_date');
            $table->date('operation_date');
            $table->float('coupon_yield', 5, 2);
            $table->unsignedTinyInteger('sell_type');
            $table->unsignedTinyInteger('output_suppression_flg');
            $table->unsignedTinyInteger('auth_status');
            $table->unsignedTinyInteger('epc_free_flg');
            $table->unsignedTinyInteger('recommend_flg');
            $table->string('module_maker', 100)->nullable();
            $table->string('module_configure', 255)->nullable();
            $table->float('maximum_output', 8, 2);
            $table->string('power_conditioner_maker', 100);
            $table->string('power_conditioner_configure', 255);
            $table->float('total_system_output', 8, 2);
            $table->string('land_price', 20)->nullable();
            $table->string('insurance', 100)->nullable();
            $table->string('monitoring', 100)->nullable();
            $table->string('maintenance', 100)->nullable();
            $table->text('memo_1')->nullable();
            $table->text('memo_2')->nullable();
            $table->text('memo_3')->nullable();
            $table->text('memo_4')->nullable();
            $table->text('memo_5')->nullable();
            $table->string('keyword', 255)->nullable();
            $table->softDeletes();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estates');
    }
}
