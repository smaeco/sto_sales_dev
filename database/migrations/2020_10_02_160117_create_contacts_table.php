<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('contact_type');
            $table->unsignedTinyInteger('purpose_type')->nullable();
            $table->unsignedTinyInteger('user_type');
            $table->string('company_name', 50);
            $table->string('personal_name', 50);
            $table->string('tel', 20);
            $table->string('email', 20);
            $table->string('address_1', 20)->nullable();
            $table->string('address_2', 20)->nullable();
            $table->string('address_3', 100)->nullable();
            $table->string('maximum_output', 20)->nullable();
            $table->string('fit_unit_price', 20)->nullable();
            $table->string('interconnection_date', 20)->nullable();
            $table->string('location')->nullable();
            $table->unsignedTinyInteger('land_contract_type')->nullable();
            $table->string('annual_energy_generation', 20)->nullable();
            $table->unsignedTinyInteger('selling_type')->nullable();
            $table->string('price', 20)->nullable();
            $table->string('asking_area', 20)->nullable();
            $table->string('asking_time_zone', 20);
            $table->text('comment')->nullable();
            $table->unsignedInteger('to_buy_estate_id')->nullable()->default(null);
            $table->unsignedTinyInteger('contact_status')->default(0);
            $table->unsignedInteger('contact_staff')->nullable()->default(null);
            $table->softDeletes();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
