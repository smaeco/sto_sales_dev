@include("business.layouts.common")
@include("business.layouts.navi")

    <div class="container">

      <h1 class="mt-4 mb-3">
        <small>お問い合わせ確認</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('top.index') }}">TOP</a>
        </li>
        <li class="breadcrumb-item active">問い合わせ</li>
      </ol>

      <div class="container">
        <form name="sell_process" id="contactForm" novalidate action="{{ route('contact.sell.process')}}" method="post">
        @csrf

          <input type="hidden" name="contact_type" value="{{ $input['contact_type'] }}">
          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">種別<span class="badge badge-danger ml-1">必須</span></p>
            <div class="col-sm-8">
            {{ config('const.CONTACT.USER_TYPE')[$input['user_type']] }}
            </div>
          </div>
          <input type="hidden" name="user_type" value="{{ $input['user_type'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">企業名<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['company_name'] }}
              </div>
          </div>
          <input type="hidden" name="company_name" value="{{ $input['company_name'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">お名前<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['personal_name'] }}
              </div>
          </div>
          <input type="hidden" name="personal_name" value="{{ $input['personal_name'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">電話番号<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['tel'] }}
              </div>
          </div>
          <input type="hidden" name="tel" value="{{ $input['tel'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">メールアドレス<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['email'] }}
              </div>
          </div>
          <input type="hidden" name="email" value="{{ $input['email'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">ご住所</p>
              <div class="col-sm-8">
              {{ $input['address_1'] }}
              {{ $input['address_2'] }}
              {{ $input['address_3'] }}
              </div>
          </div>
          <input type="hidden" name="address_1" value="{{ $input['address_1'] }}">
          <input type="hidden" name="address_2" value="{{ $input['address_2'] }}">
          <input type="hidden" name="address_3" value="{{ $input['address_3'] }}">

          <hr>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">最大出力</p>
              <div class="col-sm-8">
              {{ $input['maximum_output'] }}
              </div>
          </div>
          <input type="hidden" name="maximum_output" value="{{ $input['maximum_output'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">売電単価</p>
              <div class="col-sm-8">
              {{ $input['fit_unit_price'] }}
              </div>
          </div>
          <input type="hidden" name="fit_unit_price" value="{{ $input['fit_unit_price'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">連系時期</p>
              <div class="col-sm-8">
              {{ $input['interconnection_date'] }}
              </div>
          </div>
          <input type="hidden" name="interconnection_date" value="{{ $input['interconnection_date'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">所在地</p>
              <div class="col-sm-8">
              {{ $input['location'] }}
              </div>
          </div>
          <input type="hidden" name="location" value="{{ $input['location'] }}">

          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">土地契約</p>
            <div class="col-sm-8">
            @if (!empty($input['land_contract_type']))
            {{ config('const.ESTATE.LAND_CONTRACT_TYPE')[$input['land_contract_type']] }}
            <input type="hidden" name="land_contract_type" value="{{ $input['land_contract_type'] }}">
            @endif
            </div>
          </div>

          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">年間発電量</p>
            <div class="col-sm-8">
            {{ $input['annual_energy_generation'] }}
            </div>
          </div>
          <input type="hidden" name="annual_energy_generation" value="{{ $input['annual_energy_generation'] }}">

          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">売却形態</p>
            <div class="col-sm-8">
            @if (!empty($input['selling_type']))
            {{ config('const.CONTACT.SELLING_TYPE')[$input['selling_type']] }}
            @endif
            </div>
          </div>
          <input type="hidden" name="selling_type" value="{{ $input['selling_type'] }}">

          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">希望価格</p>
            <div class="col-sm-8">
            {{ $input['price'] }}
            </div>
          </div>
          <input type="hidden" name="price" value="{{ $input['price'] }}">

          <hr>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">希望連絡時間帯<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['asking_time_zone'] }}
              </div>
          </div>
          <input type="hidden" name="asking_time_zone" value="{{ $input['asking_time_zone'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">お問い合わせ内容<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['comment'] }}
              </div>
          </div>
          <input type="hidden" name="comment" value="{{ $input['comment'] }}">

          <div class="control-group form-group">
            <div class="text-center">
                <button name="action" type="submit" class="btn btn-dark" id="retuenContactButton" value="return">入力画面に戻る</button>
                <button name="action" type="submit" class="btn btn-primary" id="sendContactButton" value="submit">送信</button>
            </div>
          </div>
        </form>
      </div>
    </div>

@include("business.layouts.footer")