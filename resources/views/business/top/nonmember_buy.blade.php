@include("business.layouts.common")
@include("business.layouts.navi")

  <!-- Page Content -->
  <div class="container">

    <h1 class="my-4">非会員ページ</h1>

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{ route('top.index') }}">TOP</a>
      </li>
      <li class="breadcrumb-item active">購入・投資</li>
    </ol>

    <img class="img-fluid rounded mb-4" src="http://placehold.it/1200x450?text=Buy Message" alt="">
    <img class="img-fluid rounded mb-4" src="http://placehold.it/1200x200?text=Flow" alt="">

    <div class="text-center">
      <a class="btn btn-danger btn-lg m-2" href="{{ route('register') }}">会員登録</a>
      <a class="btn btn-success btn-lg m-2" href="{{ route('contact.buy.input') }}">問い合わせ</a>
    </div>

    <h2 class="my-4">物件紹介</h2>

    <div class="row">
      <div class="col-md-5">
        <a href="#">
          <img class="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/400x300?text=Estate Image" alt="">
        </a>
      </div>
      <div class="col-md-7">
        <h3>セカンダリー蒲生</h3>
        <p>鹿児島県姶良市蒲生町上久徳2259-1</p>
        <table class="table">
          <tr>
              <th class="table-active">物件価格</th>
              <td>5,000,000,000円</td>
              <th class="table-active">表面利回り</th>
              <td>13.58%</td>
          </tr>
          <tr>
              <th class="table-active">売電単価</th>
              <td>40円</td>
              <th class="table-active">システム容量</th>
              <td>2000.58kw</td>
          </tr>
        </table>
        <a class="btn btn-success" href="{{ route('contact.buy.input') }}">問い合わせ
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-5">
        <a href="#">
          <img class="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/400x300?text=Estate Image" alt="">
        </a>
      </div>
      <div class="col-md-7">
        <h3>セカンダリー蒲生</h3>
        <p>鹿児島県姶良市蒲生町上久徳2259-1</p>
        <table class="table">
          <tr>
              <th class="table-active">物件価格</th>
              <td>5,000,000,000円</td>
              <th class="table-active">表面利回り</th>
              <td>13.58%</td>
          </tr>
          <tr>
              <th class="table-active">売電単価</th>
              <td>40円</td>
              <th class="table-active">システム容量</th>
              <td>2000.58kw</td>
          </tr>
        </table>
        <a class="btn btn-success" href="{{ route('contact.buy.input') }}">問い合わせ
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
    </div>
    <hr>

    <div class="card mb-4">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-6">
              <img class="img-fluid rounded m-2" src="http://placehold.it/700x300" alt="">
          </div>
          <div class="col-lg-6">
            <h2 class="card-title">会員様には物件情報をご紹介します</h2>
            <p class="card-text">会員登録をして頂くと、物件情報の閲覧・検索ができます</p>
            <a class="btn btn-danger btn-lg m-2" href="{{ route('register') }}">会員登録</a>
            <a class="btn btn-success btn-lg m-2" href="{{ route('contact.other.input') }}">問い合わせ</a>
          </div>
        </div>
      </div>
    </div>

@include("business.layouts.footer")