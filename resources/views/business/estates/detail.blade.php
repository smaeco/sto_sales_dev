@include("business.layouts.common")
@include("business.layouts.navi")

  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">物件詳細</h1>

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{ route('top.index') }}">TOP</a>
      </li>
      <li class="breadcrumb-item active">物件詳細</li>
    </ol>

    <!-- Intro Content -->
    <div class="row">
      <div class="col-lg-5">
        <img class="img-fluid rounded mb-4" src="http://placehold.it/400x300" alt="">
      </div>
      <div class="col-lg-7">
        <h2>{{ $estate->estate_name }}</h2>
        <p>{{ $estate->location }}</p>
        <a class="btn btn-success mb-4" href="{{ route('contact.buy.input', ['estate_id' => $estate->id]) }}">問い合わせ
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
        <table class="table">
          <tr>
              <th class="table-active">物件価格</th><td>{{ number_format($estate->price) }}円</td>
          </tr>
          <tr>
              <th class="table-active">表面利回り</th><td>{{ $estate->coupon_yield }}%</td>
          </tr>
          <tr>
              <th class="table-active">売電単価</th><td>{{ $estate->fit_unit_price }}円</td>
          </tr>
          <tr>
              <th class="table-active">システム容量</th><td>{{ $estate->maximum_output }}kw</td>
          </tr>
          <tr>
              <th class="table-active">連系時期</th><td>{{ $estate->interconnection_date->format('Y年m月') }}</td>
          </tr>
          <tr>
              <th class="table-active">土地契約</th><td>{{ config('const.ESTATE.LAND_CONTRACT_TYPE')[$estate->land_contract_type] }}</td>
          </tr>
        </table>
      </div>
    </div>
    <!-- /.row -->

    <hr>

    <h3>発電概要</h3>

    <div class="row">
      <div class="col-lg-12">
        <table class="table">
          <tr>
              <th class="table-active">日射量</th><td>{{ $estate->irradiation }}kWh/㎥</td>
              <th class="table-active">土地面積</th><td>{{ $estate->area }}ha</td>
          </tr>
          <tr>
              <th class="table-active">電力区分</th><td>{{ config('const.ESTATE.VOLTAGE_TYPE')[$estate->voltage_type] }}</td>
              <th class="table-active">年間発電量</th><td>{{ $estate->annual_energy_generation }}Kwh</td>
          </tr>
          <tr>
              <th class="table-active">年間売電収入</th><td>{{ number_format($estate->annual_energy_income) }}万円</td>
              <th class="table-active">運転開始時期</th><td>{{ $estate->operation_date->format('Y年m月') }}</td>
          </tr>
          <tr>
              <th class="table-active">売買区分</th><td>{{ $estate->sell_type }}kw</td>
              <th class="table-active">出力抑制</th><td>{{ config('const.ESTATE.OUTPUT_SUPPRESSION')[$estate->output_suppression_flg] }}</td>
          </tr>
          <tr>
              <th class="table-active">認可取得状況</th><td>{{ config('const.ESTATE.AUTH_STATUS')[$estate->auth_status] }}</td>
              <th class="table-active">EPCフリー</th><td>{{ config('const.ESTATE.EPC_FREE')[$estate->epc_free_flg] }}</td>
          </tr>
        </table>
      </div>
    </div>

    <hr>

    <h3>システム概要</h3>

    <div class="row">
      <div class="col-lg-12">
        <table class="table">
          <tr>
              <th class="table-active">モジュールメーカー</th><td>{{ $estate->module_maker }}</td>
              <th class="table-active">モジュール構成</th><td>{{ $estate->module_configure }}</td>
          </tr>
          <tr>
              <th class="table-active">パワコンメーカー</th><td>{{ $estate->power_conditioner_maker }}</td>
              <th class="table-active">パワコン構成</th><td>{{ $estate->power_conditioner_configure }}</td>
          </tr>
          <tr>
              <th class="table-active">パワコン合計出力</th><td>{{ $estate->total_system_output }}Kw</td>
          </tr>
        </table>
      </div>
    </div>

    <hr>

    <h3>その他</h3>

    <div class="row">
      <div class="col-lg-12">
        <table class="table">
          <tr>
              <th class="table-active">土地代賃料</th><td>{{ $estate->land_price }}</td>
              <th class="table-active">保険</th><td>{{ $estate->insurance }}</td>
          </tr>
          <tr>
              <th class="table-active">監視</th><td>{{ $estate->monitoring }}</td>
              <th class="table-active">メンテナンス</th><td>{{ $estate->maintenance }}</td>
          </tr>
          <tr>
              <th class="table-active">その他特記事項</th>
              <td>{{ $estate->memo_1 }}<br>{{ $estate->memo_2 }}<br>{{ $estate->memo_3 }}<br>{{ $estate->memo_4 }}<br>{{ $estate->memo_5 }}</td>
          </tr>
        </table>
      </div>
    </div>
    
    <div class="text-center mb-4">
      <a class="btn btn-success btn-lg m-2" href="{{ route('contact.buy.input') }}">問い合わせ</a>
    </div>

    <div class="card mb-4">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-5">
              <img class="img-fluid rounded m-2" src="http://placehold.it/700x300" alt="">
          </div>
          <div class="col-lg-7">
            <h2 class="card-title">TEL:03-1234-5678</h2>
            <p class="card-text">受付：月～金 10:00～18:00</p>
            <a class="btn btn-lg btn-secondary" href="#">お電話</a>
          </div>
        </div>
      </div>
    </div>


  </div>
  <!-- /.container -->

@include("business.layouts.footer")
