@include("admin.layouts.header")
@include("admin.layouts.navi")
@include("admin.layouts.sidebar")

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">物件詳細</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}">TOP</a></li>
              <li class="breadcrumb-item active">物件詳細</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
          @include("admin.layouts.message")
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <h5>物件概要</h5>
                <table class="table table-bordered" style="table-layout:fixed;">
                    <tr>
                      <th class="table-active">ID</th>
                      <td colspan="3">{{ $estate->id }}</td>
                    </tr>
                    <tr>
                      <th class="table-active">物件名</th>
                      <td colspan="3">{{ $estate->estate_name }}</td>
                    </tr>
                    <tr>
                      <th class="table-active">所在地</th>
                      <td colspan="3">{{ $estate->location }}</td>
                    </tr>
                    <tr>
                      <th class="table-active">価格</th>
                      <td>{{ number_format($estate->price) }}円</td>
                      <th class="table-active">表面利回り</th>
                      <td>{{ $estate->coupon_yield }}%</td>
                    </tr>
                    <tr>
                      <th class="table-active">売電単価</th>
                      <td>{{ $estate->fit_unit_price }}円</td>
                      <th class="table-active">システム容量</th>
                      <td>{{ $estate->maximum_output }}kw</td>
                    </tr>
                    <tr>
                      <th class="table-active">連系時期</th>
                      <td>{{ $estate->interconnection_date->format('Y年m月') }}</td>
                      <th class="table-active">土地契約</th>
                      <td>{{ config('const.ESTATE.LAND_CONTRACT_TYPE')[$estate->land_contract_type] }}</td>
                    </tr>
                </table>
              </div>

              <div class="card-body">
                <h5>発電概要</h5>
                <table class="table table-bordered" style="table-layout:fixed;">
                    <tr>
                        <th class="table-active">日射量</th><td>{{ $estate->irradiation }}kWh/㎥</td>
                        <th class="table-active">土地面積</th><td>{{ $estate->area }}ha</td>
                    </tr>
                    <tr>
                        <th class="table-active">電力区分</th><td>{{ config('const.ESTATE.VOLTAGE_TYPE')[$estate->voltage_type] }}</td>
                        <th class="table-active">年間発電量</th><td>{{ $estate->annual_energy_generation }}Kwh</td>
                    </tr>
                    <tr>
                        <th class="table-active">年間売電収入</th><td>{{ number_format($estate->annual_energy_income) }}万円</td>
                        <th class="table-active">運転開始時期</th><td>{{ $estate->operation_date->format('Y年m月') }}</td>
                    </tr>
                    <tr>
                        <th class="table-active">売買区分</th><td>{{ $estate->sell_type }}</td>
                        <th class="table-active">出力抑制</th><td>{{ config('const.ESTATE.OUTPUT_SUPPRESSION')[$estate->output_suppression_flg] }}</td>
                    </tr>
                    <tr>
                        <th class="table-active">認可取得状況</th><td>{{ config('const.ESTATE.AUTH_STATUS')[$estate->auth_status] }}</td>
                        <th class="table-active">EPCフリー</th><td>{{ config('const.ESTATE.EPC_FREE')[$estate->epc_free_flg] }}</td>
                    </tr>
                    <tr>
                        <th class="table-active">おすすめ物件フラグ</th><td>{{ config('const.ESTATE.OUTPUT_SUPPRESSION')[$estate->recommend_flg] }}</td>
                    </tr>
                </table>
              </div>

              <div class="card-body">
                <h5>システム概要</h5>
                <table class="table table-bordered" style="table-layout:fixed;">
                    <tr>
                        <th class="table-active">モジュールメーカー</th><td>{{ $estate->module_maker }}</td>
                        <th class="table-active">モジュール構成</th><td>{{ $estate->module_configure }}</td>
                    </tr>
                    <tr>
                        <th class="table-active">パワコンメーカー</th><td>{{ $estate->power_conditioner_maker }}</td>
                        <th class="table-active">パワコン構成</th><td>{{ $estate->power_conditioner_configure }}</td>
                    </tr>
                    <tr>
                        <th class="table-active">パワコン合計出力</th><td>{{ $estate->total_system_output }}Kw</td>
                    </tr>
                </table>
              </div>

              <div class="card-body">
                <h5>その他</h5>
                <table class="table table-bordered" style="table-layout:fixed;">
                    <tr>
                        <th class="table-active">土地代賃料</th><td>{{ $estate->land_price }}</td>
                        <th class="table-active">保険</th><td>{{ $estate->insurance }}</td>
                    </tr>
                    <tr>
                        <th class="table-active">監視</th><td>{{ $estate->monitoring }}</td>
                        <th class="table-active">メンテナンス</th><td>{{ $estate->maintenance }}</td>
                    </tr>
                    <tr>
                        <th class="table-active">その他特記事項</th>
                        <td colspan="3">{{ $estate->memo_1 }}<br>{{ $estate->memo_2 }}<br>{{ $estate->memo_3 }}<br>{{ $estate->memo_4 }}<br>{{ $estate->memo_5 }}</td>
                    </tr>
                </table>
              </div>
            </div>
            <!-- /.card -->

          </div>
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
  </div>

@include("admin.layouts.footer")
