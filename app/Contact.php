<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $table = 'contacts';

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    protected $fillable = [
        'contact_type',
        'purpose_type',
        'user_type',
        'company_name',
        'personal_name',
        'tel',
        'email',
        'address_1',
        'address_2',
        'address_3',
        'maximum_output',
        'fit_unit_price',
        'interconnection_date',
        'location',
        'land_contract_type',
        'annual_energy_generation',
        'selling_type',
        'price',
        'asking_area',
        'asking_time_zone',
        'comment',
        'to_buy_estate_id',
        'contact_status',
        'contact_staff',        
    ];
}
