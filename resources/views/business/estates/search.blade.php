    <h2>物件検索</h2>

    <!-- Content Row -->
    <div class="row">
      <!-- Map Column -->
      <div class="col-lg-6 mb-4">
        <!-- Embedded Google Map -->
        <iframe style="width: 100%; height: 400px; border: 0;" src="http://maps.google.com/maps?hl=en&amp;ie=UTF8&amp;ll=37.509747,137.62834&amp;spn=56.506174,79.013672&amp;t=m&amp;z=4&amp;output=embed"></iframe>
      </div>
      <!-- Contact Details Column -->
      <div class="col-lg-6 mb-4">
        <form name="search" id="contactForm" novalidate action="{{ route('estate.list')}}" method="get">

          <!-- 【テスト用】地域のID -->
          <input type="hidden" name="prefecture_id[0]" value="46">
          <input type="hidden" name="prefecture_id[1]" value="47">
          <!-- //【テスト用】地域のID -->

          <div class="control-group form-group">
            <div class="controls">
              <label>電圧:</label>
              <div class="radio-btn">
                @foreach(config('const.ESTATE.VOLTAGE_TYPE') as $key => $value)
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="voltage_type{{ $key }}" name="voltage_type" class="custom-control-input" value="{{ $key }}">
                  <label class="custom-control-label" for="voltage_type{{ $key }}">{{ $value }}</label>
                </div>
                @endforeach              
              </div>
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>価格:</label>
              <div class="form-row">
                <div class="col">
                  <select class="form-control" id="price_lower" name="price_lower">
                    @foreach(config('const.ESTATE.PRICE_LOWER') as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach              
                  </select>
                </div>
                ～
                <div class="col">
                  <select class="form-control" id="price_upper" name="price_upper">
                    @foreach(config('const.ESTATE.PRICE_UPPER') as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach              
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <div class="form-row">
                <div class="col">
                  <label>表面利回り:</label>
                  <select class="form-control" id="coupon_yield" name="coupon_yield">
                    @foreach(config('const.ESTATE.COUPON_YIELD') as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach              
                  </select>
                </div>
                <div class="col">
                  <label>FIT:</label>
                  <select class="form-control" id="fit_unit_price" name="fit_unit_price">
                    @foreach(config('const.ESTATE.FIT') as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach              
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>連系時期:</label>
              <div class="form-row">
                <div class="col">
                  <input class="form-control" type="month" value="" id="interconnection_date_start" name="interconnection_date_start">
                </div>
                ～
                <div class="col">
                  <input class="form-control" type="month" value="" id="interconnection_date_end" name="interconnection_date_end">
                </div>
              </div>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>キーワード:</label>
              <input type="text" class="form-control" id="keyword" name="keyword">
            </div>
          </div>
          <div id="success"></div>
          <button type="submit" class="btn btn-primary" id="sendSearchButton">検索する</button>
        </form>
      </div>
    </div>
    <!-- /.row -->
