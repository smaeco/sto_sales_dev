# 太陽光取引システム

## ミドルウェア

* PHP 7.4
* mysql Ver 14.14 Distrib 5.7.31
* Laravel 6

## 参考テンプレート

* ユーザ画面  
https://startbootstrap.com/templates/modern-business/

* 管理画面  
https://adminlte.io/themes/v3/


## ディレクトリ

* Docker  
  `/solar_power/docker`  
  `/solar_power/docker-compose.yml`

* ソースコード  
  `/solar_power/src/`

## 構築手順

1. Docker環境をクローン  
https://bitbucket.org/no1s_lab/solar_power/src/master/

2. ```docker/app/000-default.conf```を修正  
```
<VirtualHost *:80>  
       ServerAdmin webmaster@localhost  
       DocumentRoot /var/www/html/solarpower/public  
       ErrorLog ${APACHE_LOG_DIR}/error.log  
       CustomLog ${APACHE_LOG_DIR}/access.log combined  
       <Directory /var/www/html/solarpower/public>  
           AllowOverride All  
       </Directory>  
</VirtualHost>  
```

3. 太陽光システムのソースをクローン  
solar_power/src/ へ移動  
こちらからクローン  
https://bitbucket.org/smaeco/solarpower/src/master/

4. Dockerの起動
```
$ cd solar_power
$ docker-compose build --no-cache
$ docker-compose up -d
$ docker-compose down
```

5. ターミナルソフトでログイン
```
ホスト名	192.168.99.100
ログインID	docker
パスワード	tcuser
```
```
$ docker ps
$ docker exec -it laravel_app bash
$ cd solarpower/
```

6. developブランチを選択
```
$ git fetch
$ git checkout -b develop origin/develop
```

7. composerを実行
```
$ composer install

8. .env ファイルを作成
```
cp .env.example .env
```

9. データベース作成（ターミナルで別タブで接続）
```
$ docker exec -it laravel_db bash
# mysql -u root -p
mysql> CREATE DATABASE solarpower;
mysql> CREATE USER no1s_user IDENTIFIED BY 'P8idBYSW';
mysql> GRANT ALL On solarpower.* To no1s_user;
```

10. laravel_appに戻って、migrationを実行
```
$ php artisan migrate
```

## 動作確認

* ユーザ画面  
http://192.168.99.100:8080/

* 管理画面  
http://192.168.99.100:8080/admin

## Tips

* キャッシュクリアコマンド  
https://qiita.com/Ping/items/10ada8d069e13d729701