@include("business.layouts.common")
@include("business.layouts.navi")

  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">物件一覧</h1>

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{ route('top.index') }}">TOP</a>
      </li>
      <li class="breadcrumb-item active">物件一覧</li>
    </ol>

    <h2>{{ $paging['count'] }}件</h2> {{ $paging['from'] }}件 ～ {{ $paging['to'] }}件

    <div class="row">
      <div class="col-lg-12">
        <table class="table">
          <tr>
              @if ($selected_prefecture_name)
              <th>地域</th>
              @endif
              <td>{{ $selected_prefecture_name }}</td>
              @if (!empty($params['voltage_type']))
              <th>電圧</th><td>{{ config('const.ESTATE.VOLTAGE_TYPE')[$params['voltage_type']] }}</td>
              @endif
          </tr>
          <tr>
              @if (!empty($params['price_lower']) || !empty($params['price_upper']))
              <th>価格</th><td>{{ config('const.ESTATE.PRICE_LOWER')[$params['price_lower']] }} ～ {{ config('const.ESTATE.PRICE_UPPER')[$params['price_upper']] }}</td>
              @endif
              @if (!empty($params['interconnection_date_start']) || !empty($params['interconnection_date_end']))
              <th>連系時期</th><td>{{ str_replace("-","/", $params['interconnection_date_start']) }} ～ {{ str_replace("-","/", $params['interconnection_date_end']) }}</td>
              @endif
          </tr>
          <tr>
              @if (!empty($params['coupon_yield']))
              <th>表面利回り</th><td>{{ config('const.ESTATE.COUPON_YIELD')[$params['coupon_yield']] }}</td>
              @endif
              @if (!empty($params['fit_unit_price']))
              <th>FIT</th><td>{{ config('const.ESTATE.FIT')[$params['fit_unit_price']] }}</td>
              @endif
          </tr>
          <tr>
              @if (!empty($params['keyword']))
              <th>キーワード</th><td>{{ $params['keyword'] }}</td>
              @endif
          </tr>
        </table>
      </div>
    </div>

    <ul class="pagination justify-content-center">
    {{ $estates->appends($params)->links() }}
    </ul>

    <!-- Project One -->
    @foreach($estates as $estate)
    <div class="row">
      <div class="col-md-5">
        <a href="#">
          <img class="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/400x300" alt="">
        </a>
      </div>
      <div class="col-md-7">
        <h3>{{ $estate->estate_name }}</h3>
        <p>{{ $estate->location }}</p>
        <table class="table">
          <tr>
              <th class="table-active">物件価格</th>
              <td>{{ number_format($estate->price) }}円</td>
              <th class="table-active">表面利回り</th>
              <td>{{ $estate->coupon_yield }}%</td>
          </tr>
          <tr>
              <th class="table-active">売電単価</th>
              <td>{{ $estate->fit_unit_price }}円</td>
              <th class="table-active">システム容量</th>
              <td>{{ $estate->maximum_output }}kw</td>
          </tr>
          <tr>
              <th class="table-active">連系時期</th>
              <td>{{ $estate->interconnection_date->format('Y年m月') }}</td>
              <th class="table-active">土地契約</th>
              <td>{{ config('const.ESTATE.LAND_CONTRACT_TYPE')[$estate->land_contract_type] }}</td>
          </tr>
        </table>
        <a class="btn btn-warning" href="{{ route('estate.show', ['estateId' => $estate->id]) }}">詳細
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
        <a class="btn btn-success" href="{{ route('contact.buy.input', ['estate_id' => $estate->id]) }}">問い合わせ
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
    </div>
    <!-- /.row -->

    <hr>
    @endforeach

    <!-- Pagination -->
    <!--
    <ul class="pagination justify-content-center">
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <li class="page-item">
        <a class="page-link" href="#">1</a>
      </li>
      <li class="page-item">
        <a class="page-link" href="#">2</a>
      </li>
      <li class="page-item">
        <a class="page-link" href="#">3</a>
      </li>
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
    -->

    <ul class="pagination justify-content-center">
    {{ $estates->appends($params)->links() }}
    </ul>

@include("business.estates.search")

  </div>
  <!-- /.container -->

@include("business.layouts.footer")
