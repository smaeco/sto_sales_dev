<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;
use DB;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $memberQuery = Member::query()
        ->when(request('company_name'), function ($query) {
            return $query->where('company_name', 'like', '%'.request('company_name').'%');
        })
        ->when(request('personal_name'), function ($query) {
            return $query->where('personal_name', 'like', '%'.request('personal_name').'%');
        })
        ->when(request('tel'), function ($query) {
            return $query->where('tel', request('tel'));
        })
        ->when(request('email'), function ($query) {
            return $query->where('email', request('email'));
        })
        ->when(request('member_status'), function ($query) {
            return $query->whereIn('member_status', request('member_status'));
        })
        ->when(request('customer_type'), function ($query) {
            return $query->whereIn('customer_type', request('customer_type'));
        });
        $members = $memberQuery->paginate(10);
        $params = [
            'company_name'   => request('company_name'),
            'personal_name'  => request('personal_name'),
            'tel'            => request('tel'),
            'email'          => request('email'),
            'member_status'  => request('member_status'),
            'customer_type'  => request('customer_type'),
        ];

        return view('admin.member.index', compact('members', 'params'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::find($id);
        return view('admin.member.show', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);
        return view('admin.member.edit', compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'member_status' => 'required',
            'customer_type' => 'required',
            'company_name'  => 'required',
            'personal_name' => 'required',
            'tel'           => 'required',
            'email'         => 'required|email',
        ]);
        $member = Member::findOrFail($id);
        $member->member_status = $request->input('member_status');
        $member->customer_type = $request->input('customer_type');
        $member->company_name  = $request->input('company_name');
        $member->personal_name = $request->input('personal_name');
        $member->tel           = $request->input('tel');
        $member->email         = $request->input('email');
        $member->address_1     = $request->input('address_1');
        $member->address_2     = $request->input('address_2');
        $member->address_3     = $request->input('address_3');

        if ($member->save()) {
            $request->session()->flash('success', '会員情報を更新しました');
        } else {
            $request->session()->flash('error', '更新に失敗しました');
        }
        return redirect()->route('admin.member.show', ['memberId' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
