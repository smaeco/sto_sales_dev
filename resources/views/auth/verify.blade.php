@include("business.layouts.common")
@include("business.layouts.navi")

<div class="container py-4">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">メールをご確認ください</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                        新しい確認リンクがあなたのメールアドレスに送信されました。
                        </div>
                    @endif

                    続行する前に、確認リンクについてメールを確認してください。
                    メールが届かない場合は、
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">こちらをクリックして別のメールをリクエストしてください</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
