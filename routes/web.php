<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  'TopController@index')->name('top.index');
Route::get('/home',  'HomeController@index')->name('home');
Route::get('/sell',  'TopController@sell')->name('top.sell');
Route::get('/buy',  'TopController@buy')->name('top.buy');

/* ユーザ画面認証 */
Auth::routes(['verify' => true]);

/* 物件検索・詳細 */
Route::get('/estate/search',  'EstateController@search')->name('estate.search');
Route::get('/estate/list',  'EstateController@list')->name('estate.list');
Route::get('/estate/detail/{estateId}',  'EstateController@show')->name('estate.show');

/* 問い合わせ */
Route::get('/contact/sell',  'ContactController@sellInput')->name('contact.sell.input');
Route::post('/contact/sell/confirm',  'ContactController@sellConfirm')->name('contact.sell.confirm');
Route::post('/contact/sell/process',  'ContactController@sellProcess')->name('contact.sell.process');
Route::get('/contact/sell/complete',  'ContactController@sellComplete')->name('contact.sell.complete');
Route::get('/contact/buy',  'ContactController@buyInput')->name('contact.buy.input');
Route::post('/contact/buy/confirm',  'ContactController@buyConfirm')->name('contact.buy.confirm');
Route::post('/contact/buy/process',  'ContactController@buyProcess')->name('contact.buy.process');
Route::get('/contact/buy/complete',  'ContactController@buyComplete')->name('contact.buy.complete');
Route::get('/contact/other',  'ContactController@otherInput')->name('contact.other.input');
Route::post('/contact/other/confirm',  'ContactController@otherConfirm')->name('contact.other.confirm');
Route::post('/contact/other/process',  'ContactController@otherProcess')->name('contact.other.process');
Route::get('/contact/other/complete',  'ContactController@otherComplete')->name('contact.other.complete');

/* Admin 認証不要 */
Route::group(['prefix' => 'admin'], function() {
    Route::get('/',         function () { return redirect('/admin'); });
    Route::get('login',     'Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('login',    'Admin\LoginController@login');
});

/* Admin ログイン後 */
Route::group(['prefix' => 'admin', 'middleware' => 'auth:staff'], function() {
    Route::get('logout',   'Admin\LoginController@logout')->name('admin.logout');
    Route::get('/',      'Admin\HomeController@index')->name('admin.home.index');
    Route::get('/member',  'Admin\MemberController@index')->name('admin.member.index');
    Route::get('/member/detail/{memberId}',  'Admin\MemberController@show')->name('admin.member.show');
    Route::get('/member/edit/{memberId}',  'Admin\MemberController@edit')->name('admin.member.edit');
    Route::post('/member/update/{memberId}',  'Admin\MemberController@update')->name('admin.member.update');
    Route::get('/estate',  'Admin\EstateController@index')->name('admin.estate.index');
    Route::get('/estate/detail/{estateId}',  'Admin\EstateController@show')->name('admin.estate.show');
    Route::get('/estate/new',  'Admin\EstateController@create')->name('admin.estate.new');
    Route::post('/estate/create',  'Admin\EstateController@store')->name('admin.estate.create');
});