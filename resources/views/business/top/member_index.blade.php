@include("business.layouts.common")
@include("business.layouts.header")

  <!-- Page Content -->
  <div class="container">

    <h1 class="my-4">会員専用ページ</h1>

    <div class="row">
      <div class="col-lg-6 portfolio-item">
        <div class="card h-100">
          <a href="/sell"><img class="card-img-top" src="http://placehold.it/468x120?text=Sell" alt=""></a>
        </div>
      </div>
      <div class="col-lg-6 portfolio-item">
        <div class="card h-100">
          <a href="/buy"><img class="card-img-top" src="http://placehold.it/468x120?text=Buy" alt=""></a>
        </div>
      </div>
    </div>

@include("business.estates.recommend")
@include("business.estates.search")

    @can('normal-member')
    <div class="card mb-4">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-6">
            <a href="#">
              <img class="img-fluid rounded" src="http://placehold.it/750x300" alt="">
            </a>
          </div>
          <div class="col-lg-6">
            <h2 class="card-title">優良会員には非公開物件をご紹介</h2>
            <p class="card-text">来社して頂き、秘密保持契約を結んで下さった皆さまは優良会員として、非公開物件をご紹介します</p>
            <a class="btn btn-success mb-2" href="{{ route('contact.other.input') }}">問い合わせ</a>
          </div>
        </div>
      </div>
    </div>
    @endcan

    <h2>新着ニュース</h2>

    <div class="m-4">
      <ul class="list-group list-group-flush col-lg-12">
        <li class="list-group-item"><span class="mx-4">2020/06/29</span><a href="#">新型コロナウイルスの感染拡大防止に伴うお問い合わせ方法変更について</a></li>
        <li class="list-group-item"><span class="mx-4">2020/05/19</span><a href="#">物件入荷しました！！</a></li>
        <li class="list-group-item"><span class="mx-4">2020/04/09</span><a href="#">物件入荷しました！！</a></li>
        <li class="list-group-item text-right"><a href="#">もっと見る</a></li>
      </ul>
    </div>

    <div class="row">
      <div class="col-lg-6 mb-4">
        <h2>太陽光私設取引について</h2>
        <p>LFIT期間が終了したあとは売電ができなくなるのではないかと不安な人も多いのではないでしょうか。
          この記事では、「卒FITとはどういうことなのか」「新電力への売電はメリットがあるのか」について解説します。
          この記事を読むことによって、FIT期間が終わったあとの不安もなくなり、余剰電力をお得に活用する方法がわかります。</p>
      </div>
      <div class="col-lg-6 mb-4">
        <img class="img-fluid rounded" src="http://placehold.it/700x300" alt="">
      </div>
    </div>
    <!-- /.row -->

    <h2>おすすめ情報</h2>

    <div class="row">
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Info" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Info" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Info" alt="">
      </div>
      <div class="col-lg-3 col-sm-4 mb-4">
        <img class="img-fluid" src="http://placehold.it/500x150?text=Info" alt="">
      </div>
    </div>

@include("business.layouts.footer")