<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Authenticatable implements MustVerifyEmailContract
{
    use MustVerifyEmail, Notifiable;
    use SoftDeletes;

    protected $table = 'members';

    protected $dates = ['email_verified_at', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_status', 
        'customer_type', 
        'company_name', 
        'personal_name', 
        'tel', 
        'email',
        'email_verified_at',
        'address_1', 
        'address_2', 
        'address_3', 
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
     protected $hidden = [
        'password', 'remember_token',
    ];

}
