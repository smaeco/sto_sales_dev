  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="{{ route('top.index') }}">太陽光私設取引システム</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('top.sell') }}">売却・転売</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('top.buy') }}">購入・投資</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/">お役立ち情報</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/">お取引の流れ</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>