@include("admin.layouts.header")
@include("admin.layouts.navi")
@include("admin.layouts.sidebar")

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">物件追加</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}">TOP</a></li>
              <li class="breadcrumb-item active">物件追加</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <form method="POST" action="{{ route('admin.estate.create')}}">
        @csrf
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                @if ($errors->any())
                  <div class="callout callout-danger">
                    @foreach (array_unique($errors->all()) as $error)
                    <p class="text-danger" role="alert">※{{ $error }}</p>
                    @endforeach
                  </div>
                @endif
                <table class="table table-bordered">
                  <tr>
                    <th class="table-active">物件名</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('estate_name') ? ' is-invalid':'' }}" name="estate_name" value="{{ old('estate_name') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">所在地</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('location') ? ' is-invalid':'' }}" name="location" value="{{ old('location') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">都道府県</th>
                    <td>
                      <select class="form-control" id="prefecture_id" name="prefecture_id">
                      <option value="">--</option>
                      @foreach(config('const.ESTATE.PREFECTURE') as $key => $value)
                      <option value="{{ $key }}" {{ old('prefecture_id') == $key ? ' selected' : ''}}>{{ $value }}</option>
                      @endforeach              
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">価格</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('price') ? ' is-invalid':'' }}" name="price" value="{{ old('price') }}">円
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">表面利回り</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('coupon_yield') ? ' is-invalid':'' }}" name="coupon_yield" value="{{ old('coupon_yield') }}">円
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">売電単価</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('fit_unit_price') ? ' is-invalid':'' }}" name="fit_unit_price" value="{{ old('fit_unit_price') }}">%
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">システム容量</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('maximum_output') ? ' is-invalid':'' }}" name="maximum_output" value="{{ old('maximum_output') }}">kw
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">連系時期</th>
                    <td>
                      <input type="month"" class="form-control{{ $errors->has('interconnection_date') ? ' is-invalid':'' }}" name="interconnection_date" value="{{ old('interconnection_date') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">土地契約</th>
                    <td>
                      @foreach(config('const.ESTATE.LAND_CONTRACT_TYPE') as $key => $value)
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="land_contract_type{{ $key }}" name="land_contract_type" class="custom-control-input{{ $errors->has('land_contract_type') ? ' is-invalid':'' }}" value="{{ $key }}">
                        <label class="custom-control-label" for="land_contract_type{{ $key }}">{{ $value }}</label>
                      </div>
                      @endforeach
                    </td>
                  </tr>
                </table>
              </div>
              <div class="card-body">
                <table class="table table-bordered">
                  <tr>
                    <th class="table-active">日射量</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('irradiation') ? ' is-invalid':'' }}" name="irradiation" value="{{ old('irradiation') }}">
                      kWh/㎥
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">土地面積</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('area') ? ' is-area':'' }}" name="location" value="{{ old('area') }}">
                      ha
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">電力区分</th>
                    <td>
                      @foreach(config('const.ESTATE.VOLTAGE_TYPE') as $key => $value)
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="voltage_type{{ $key }}" name="voltage_type" class="custom-control-input{{ $errors->has('voltage_type') ? ' is-invalid':'' }}" value="{{ $key }}">
                        <label class="custom-control-label" for="voltage_type{{ $key }}">{{ $value }}</label>
                      </div>
                      @endforeach
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">年間発電量</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('annual_energy_generation') ? ' is-invalid':'' }}" name="annual_energy_generation" value="{{ old('annual_energy_generation') }}">Kwh
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">年間売電収入</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('annual_energy_income') ? ' is-invalid':'' }}" name="annual_energy_income" value="{{ old('annual_energy_income') }}">万円
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">運転開始時期</th>
                    <td>
                      <input type="month" class="form-control{{ $errors->has('operation_date') ? ' is-invalid':'' }}" name="operation_date" value="{{ old('operation_date') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">売買区分</th>
                    <td>
                      @foreach(config('const.CONTACT.SELLING_TYPE') as $key => $value)
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="sell_type{{ $key }}" name="sell_type" class="custom-control-input{{ $errors->has('sell_type') ? ' is-invalid':'' }}" value="{{ $key }}">
                        <label class="custom-control-label" for="sell_type{{ $key }}">{{ $value }}</label>
                      </div>
                      @endforeach
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">出力抑制</th>
                    <td>
                      @foreach(config('const.ESTATE.OUTPUT_SUPPRESSION') as $key => $value)
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="output_suppression_flg{{ $key }}" name="output_suppression_flg" class="custom-control-input{{ $errors->has('output_suppression_flg') ? ' is-invalid':'' }}" value="{{ $key }}">
                        <label class="custom-control-label" for="output_suppression_flg{{ $key }}">{{ $value }}</label>
                      </div>
                      @endforeach
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">認可取得状況</th>
                    <td>
                      @foreach(config('const.ESTATE.AUTH_STATUS') as $key => $value)
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="auth_status{{ $key }}" name="auth_status" class="custom-control-input{{ $errors->has('auth_status') ? ' is-invalid':'' }}" value="{{ $key }}">
                        <label class="custom-control-label" for="auth_status{{ $key }}">{{ $value }}</label>
                      </div>
                      @endforeach
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">EPCフリー</th>
                    <td>
                      @foreach(config('const.ESTATE.EPC_FREE') as $key => $value)
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="epc_free_flg{{ $key }}" name="epc_free_flg" class="custom-control-input{{ $errors->has('epc_free_flg') ? ' is-invalid':'' }}" value="{{ $key }}">
                        <label class="custom-control-label" for="epc_free_flg{{ $key }}">{{ $value }}</label>
                      </div>
                      @endforeach
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">おすすめ物件フラグ</th>
                    <td>
                      @foreach(config('const.ESTATE.OUTPUT_SUPPRESSION') as $key => $value)
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="recommend_flg{{ $key }}" name="recommend_flg" class="custom-control-input{{ $errors->has('recommend_flg') ? ' is-invalid':'' }}" value="{{ $key }}">
                        <label class="custom-control-label" for="recommend_flg{{ $key }}">{{ $value }}</label>
                      </div>
                      @endforeach
                    </td>
                  </tr>
                </table>
              </div>
              <div class="card-body">
                <table class="table table-bordered">
                  <tr>
                    <th class="table-active">モジュールメーカー</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('module_maker') ? ' is-invalid':'' }}" name="module_maker" value="{{ old('module_maker') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">モジュール構成</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('module_configure') ? ' is-invalid':'' }}" name="module_configure" value="{{ old('module_configure') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">パワコンメーカー</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('power_conditioner_maker') ? ' is-invalid':'' }}" name="power_conditioner_maker" value="{{ old('power_conditioner_maker') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">パワコン構成</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('power_conditioner_configure') ? ' is-invalid':'' }}" name="power_conditioner_configure" value="{{ old('power_conditioner_configure') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">パワコン合計出力</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('total_system_output') ? ' is-invalid':'' }}" name="total_system_output" value="{{ old('total_system_output') }}">Kw
                    </td>
                  </tr>
                </table>
              </div>
              <div class="card-body">
                <table class="table table-bordered">
                  <tr>
                    <th class="table-active">土地代賃料</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('land_price') ? ' is-invalid':'' }}" name="land_price" value="{{ old('land_price') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">保険</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('insurance') ? ' is-invalid':'' }}" name="insurance" value="{{ old('insurance') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">監視</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('monitoring') ? ' is-invalid':'' }}" name="monitoring" value="{{ old('monitoring') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">メンテナンス</th>
                    <td>
                      <input type="text" class="form-control{{ $errors->has('maintenance') ? ' is-invalid':'' }}" name="maintenance" value="{{ old('maintenance') }}">
                    </td>
                  </tr>
                  <tr>
                    <th class="table-active">その他特記事項</th>
                    <td>
                      <input type="text" class="my-2 form-control{{ $errors->has('memo_1') ? ' is-invalid':'' }}" name="memo_1" value="{{ old('memo_1') }}">
                      <input type="text" class="my-2 form-control{{ $errors->has('memo_2') ? ' is-invalid':'' }}" name="memo_2" value="{{ old('memo_2') }}">
                      <input type="text" class="my-2 form-control{{ $errors->has('memo_3') ? ' is-invalid':'' }}" name="memo_3" value="{{ old('memo_3') }}">
                      <input type="text" class="my-2 form-control{{ $errors->has('memo_4') ? ' is-invalid':'' }}" name="memo_4" value="{{ old('memo_4') }}">
                      <input type="text" class="my-2 form-control{{ $errors->has('memo_5') ? ' is-invalid':'' }}" name="memo_5" value="{{ old('memo_5') }}">
                    </td>
                  </tr>
                </table>
              </div>
              <div class="row m-2">
                <button type="submit" class="btn btn-primary m-1 w-25 btn-flat">追加</button>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        </form>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
  </div>

@include("admin.layouts.footer")
