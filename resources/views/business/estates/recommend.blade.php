
<h2>おすすめ物件</h2>

<div class="row">
@foreach($recommend_estates as $estate)
  <div class="col-lg-4 col-sm-6 portfolio-item">
    <div class="card h-70">
      <a href="#"><img class="card-img-top" src="http://placehold.it/700x400?text=Estate Image" alt=""></a>
      <div class="card-body">
        <h4 class="card-title">
          <a href="{{ route('estate.show', ['estateId' => $estate->id]) }}">{{ $estate->estate_name }}</a>
        </h4>
        <p class="card-text">価格：{{ $estate->price }}円</p>
        <p class="card-text">利回り：{{ $estate->coupon_yield }}%</p>
      </div>
    </div>
  </div>
  @endforeach
</div>
<div class="text-center">
    <a class="btn btn-success btn-lg mb-4" href="{{ route('contact.buy.input') }}">問い合わせ</a>
</div>
<!-- /.row -->
