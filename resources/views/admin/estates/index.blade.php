@include("admin.layouts.header")
@include("admin.layouts.navi")
@include("admin.layouts.sidebar")

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">物件一覧</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.home.index')}}">TOP</a></li>
              <li class="breadcrumb-item active">物件一覧</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">

              <!-- 検索フォーム -->
              <div class="card-header">
                <form method="GET" action="{{ route('admin.estate.index')}}">
                  <div class="row">
                    <div class="col-sm-6">
                      <label>物件名</label>
                      <div class="form-group input-group input-group-sm col-10">
                        <input type="text" class="form-control" name="estate_name" value="{{ $params['estate_name'] }}">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <label>電圧</label>
                      <div class="form-group input-group input-group-sm col-10">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="voltage_type0" name="voltage_type" class="custom-control-input" value="0" {{!is_array($params['voltage_type']) || $params['voltage_type'] == '0' ? ' checked' : ''}}>
                          <label class="custom-control-label" for="voltage_type0">全て</label>
                        </div>
                        @foreach(config('const.ESTATE.VOLTAGE_TYPE') as $key => $value)
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="voltage_type{{ $key }}" name="voltage_type" class="custom-control-input" value="{{ $key }}" {{$params['voltage_type'] == $key ? ' checked' : ''}}>
                          <label class="custom-control-label" for="voltage_type{{ $key }}">{{ $value }}</label>
                        </div>
                        @endforeach  
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <label>価格</label>
                      <div class="form-group input-group input-group-sm col-12">
                        <input type="text" class="form-control" name="price_lower" value="{{ $params['price_lower'] }}" placeholder="下限">
                        <input type="text" class="form-control" name="price_upper" value="{{ $params['price_upper'] }}" placeholder="上限">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <label>表面利回り</label>
                      <div class="form-group input-group input-group-sm col-12">
                        <input type="text" class="form-control" name="coupon_yield_lower" value="{{ $params['coupon_yield_lower'] }}" placeholder="下限">
                        <input type="text" class="form-control" name="coupon_yield_upper" value="{{ $params['coupon_yield_upper'] }}" placeholder="上限">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <label>地域</label>
                      <div class="form-group input-group input-group-sm col-6">
                      <select class="form-control" id="prefecture_id" name="prefecture_id">
                        <option value="">全て</option>
                        @foreach(config('const.ESTATE.PREFECTURE') as $key => $value)
                        <option value="{{ $key }}" {{$params['prefecture_id'] == $key ? ' selected' : ''}}>{{ $value }}</option>
                        @endforeach              
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <label>商談状況</label>
                      <div class="form-group input-group input-group-sm col-12">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="sales_status0" name="sales_status" class="custom-control-input" value="0" {{!is_array($params['sales_status']) || $params['sales_status'] == '0' ? ' checked' : ''}}>
                          <label class="custom-control-label" for="sales_status0">全て</label>
                        </div>
                        @foreach(config('const.ESTATE.SALES_STATUS') as $key => $value)
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="sales_status{{ $key }}" name="sales_status" class="custom-control-input" value="{{ $key }}" {{$params['sales_status'] == $key ? ' checked' : ''}}>
                          <label class="custom-control-label" for="sales_status{{ $key }}">{{ $value }}</label>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <label>キーワード</label>
                      <div class="form-group input-group input-group-sm col-10">
                        <input type="text" class="form-control" name="keyword" value="{{ $params['keyword'] }}">
                      </div>
                    </div>
                    <div class="col-sm-6 d-flex align-items-center">
                      <button type="submit" class="btn btn-primary btn-flat w-25">検索</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- //検索フォーム -->

              <div class="card-body">
                <div class="row">
                  <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr role="row">
                          <th>ID</th>
                          <th>物件名</th>
                          <th>地域</th>
                          <th>価格</th>
                          <th>利回り</th>
                          <th>営業状況</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($estates as $estate)
                        <tr role="row">
                          <td>{{ $estate->id }}</td>
                          <td>{{ $estate->estate_name }}
                            <div>
                            <span class="badge bg-warning">{{ config('const.ESTATE.VOLTAGE_TYPE')[$estate->voltage_type] }}</span>
                            <span class="badge bg-success">{{ config('const.ESTATE.LAND_CONTRACT_TYPE')[$estate->land_contract_type] }}</span>
                            </div>
                          </td>
                          <td>{{ config('const.ESTATE.PREFECTURE')[$estate->prefecture_id] }}</td>
                          <td>{{ $estate->price }}円</td>
                          <td>{{ $estate->coupon_yield }}%</td>
                          <td>{{-- config('const.ESTATE.SALES_STATUS')[$estate->estate_sales->sales_status] --}}</td>
                          <td>
                            <a href="{{ route('admin.estate.show', ['estateId' => $estate->id]) }}" type="button" class="btn btn-block btn-info btn-flat btn-sm">詳細</a>
                            <a href="{{-- route('admin.estate.edit', ['estateId' => $estate->id])--}}" type="button" class="btn btn-block btn-warning btn-flat btn-sm">編集</a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                      <tr>
                      <th>ID</th>
                          <th>物件名</th>
                          <th>地域</th>
                          <th>価格</th>
                          <th>利回り</th>
                          <th>営業状況</th>
                          <th></th>
                      </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                      <ul class="pagination justify-content-center">
                      {{ $estates->appends($params)->links() }}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
  </div>

@include("admin.layouts.footer")
