@include("business.layouts.common")

<div class="form-wrapper">
  <div class="card-body p-4">
    <form class="simple_form login" id="login" action="/login/exec" accept-charset="UTF-8" method="post">
      <div class="form-group row email required">
        <label class="col-sm-3 col-form-label required">メールアドレス</label>
        <div class="col-sm-9">
          <input class="form-control string email required" required="required" aria-required="true" type="email" name="email">
        </div>
      </div>
      <div class="form-group row password required password">
        <label class="col-sm-3 col-form-label password required">パスワード</label>
        <div class="col-sm-9">
          <input class="form-control password required" required="required" aria-required="true" type="password" name="password">
        </div>
      </div>
      <div class="text-center">
        <input type="submit" name="commit" value="ログイン" id="loginButton" class="btn btn-lg btn-success disabled" data-disable-with="ログイン" style="width: 100%;">
      </div>
      <div class="text-center my-3"><a href="/password">パスワードを忘れてしまった方はこちら</a></div>
      <hr class="my-4">
      <div class="text-center"><a href="/signup">会員登録はこちら</a></div>
    </form>
  </div>
</div>