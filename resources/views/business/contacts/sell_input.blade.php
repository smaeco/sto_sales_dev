@include("business.layouts.common")
@include("business.layouts.navi")

    <div class="container">

      <h1 class="mt-4 mb-3">
        <small>お問い合わせ</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('top.index') }}">TOP</a>
        </li>
        <li class="breadcrumb-item active">問い合わせ</li>
      </ol>

      <div class="container">
        <form name="sell_input" id="contactForm" novalidate action="{{ route('contact.sell.confirm')}}" method="post">
        @csrf

          <input type="hidden" name="contact_type" value="1">
          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">種別<span class="badge badge-danger ml-1">必須</span></p>
            <div class="col-sm-8 radio-btn">
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="user_type_1" name="user_type" class="custom-control-input{{ $errors->has('user_type') ? ' is-invalid':'' }}" value="1" {{ old('user_type')=='1' ? 'checked':'' }} >
                <label class="custom-control-label" for="user_type_1">企業</label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="user_type_2" name="user_type" class="custom-control-input{{ $errors->has('user_type') ? ' is-invalid':'' }}" value="2" {{ old('user_type')=='2' ? 'checked':'' }} >
                <label class="custom-control-label" for="user_type_2">機関投資家</label>
              </div>
              {!! $errors->first('user_type', '<div class="text-danger" role="alert">:message</div>') !!}
            </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">企業名<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                <input type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid':'' }}" id="company_name" name="company_name" value="{{ old('company_name') }}" required>
                {!! $errors->first('company_name', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">お名前<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                <input type="text" class="form-control{{ $errors->has('personal_name') ? ' is-invalid':'' }}" id="personal_name" name="personal_name" value="{{ old('personal_name') }}" required>
                {!! $errors->first('personal_name', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">電話番号<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                <input type="tel" class="form-control{{ $errors->has('tel') ? ' is-invalid':'' }}" id="tel" name="tel" value="{{ old('tel') }}" required>
                {!! $errors->first('tel', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">メールアドレス<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid':'' }}" id="email"" name="email" value="{{ old('email') }}" required>
                {!! $errors->first('email', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">ご住所</p>
              <div class="col-sm-8">
                <input type="text" class="form-control mb-1" id="address_1" name="address_1" value="{{ old('address_1') }}" placeholder="都道府県">
                <input type="text" class="form-control mb-1" id="address_2" name="address_2" value="{{ old('address_2') }}" placeholder="市区町村">
                <input type="text" class="form-control mb-1" id="address_3" name="address_3" value="{{ old('address_3') }}" placeholder="番地・建物名・部屋番号">
              </div>
          </div>

          <hr>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">最大出力</p>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="maximum_output" name="maximum_output" value="{{ old('maximum_output') }}">
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">売電単価</p>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="fit_unit_price" name="fit_unit_price" value="{{ old('fit_unit_price') }}">
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">連系時期</p>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="interconnection_date" name="interconnection_date" value="{{ old('interconnection_date') }}" placeholder="例：2020年12月">
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">所在地</p>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="location" name="location" value="{{ old('location') }}" >
              </div>
          </div>

          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">土地契約</p>
            <div class="col-sm-8 radio-btn">
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="land_contract_type_1" name="land_contract_type" class="custom-control-input" value="1" {{ old('land_contract_type')=="1" ? "checked":"" }} >
                <label class="custom-control-label" for="land_contract_type_1">賃貸</label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="land_contract_type_2" name="land_contract_type" class="custom-control-input" value="2" {{ old('land_contract_type')=="2" ? "checked":"" }}>
                <label class="custom-control-label" for="land_contract_type_2">売買</label>
              </div>
            </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">年間発電量</p>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="annual_energy_generation" name="annual_energy_generation" value="{{ old('annual_energy_generation') }}">
              </div>
          </div>

          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">売却形態</p>
            <div class="col-sm-8">
                <select class="form-control" id="selling_type" name="selling_type">
                  <option value="">お選びください</option>
                  @foreach(config('const.CONTACT.SELLING_TYPE') as $key => $value)
                  <option value="{{ $key }}" {{ old('selling_type')==$key ? "selected":"" }} >{{ $value }}</option>
                  @endforeach              
                </select>
              </div>
            </p>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">希望価格</p>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="price" name="price" value="{{ old('price') }}" >
              </div>
          </div>

          <hr>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">希望連絡時間帯<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                <input type="text" class="form-control{{ $errors->has('asking_time_zone') ? ' is-invalid':'' }}" id="asking_time_zone" name="asking_time_zone" value="{{ old('asking_time_zone') }}" required placeholder="例：9時～12時まで">
                {!! $errors->first('asking_time_zone', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">お問い合わせ内容<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                  <textarea rows="10" cols="100" class="form-control{{ $errors->has('comment') ? ' is-invalid':'' }}" id="comment" name="comment" required maxlength="999" style="resize:none">{{ old('comment') }}</textarea>
                  {!! $errors->first('comment', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group">
            <div class="text-center">
                <button type="submit" class="btn btn-primary" id="confirmContactButton">確認する</button>
            </div>
          </div>
        </form>
      </div>
    </div>

@include("business.layouts.footer")