{{ $data["company_name"] }}
{{ $data["personal_name"] }} 様

お問い合わせありがとうございます。

以下の内容で受け付けました。

メールまたはお電話にて担当の者からご連絡致します。

=================
企業名： {{ $data["company_name"] }}

お名前： {{ $data["personal_name"] }}

メールアドレス： {{ $data["email"] }}

電話番号： {{ $data["tel"] }}

希望連絡時間帯： {{ $data["asking_time_zone"] }}

お問い合わせ内容： {{ $data["comment"] }}
=================