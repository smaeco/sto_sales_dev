@include("business.layouts.common")
@include("business.layouts.navi")

    <div class="container">

      <h1 class="mt-4 mb-3">
        <small>お問い合わせ確認</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('top.index') }}">TOP</a>
        </li>
        <li class="breadcrumb-item active">問い合わせ</li>
      </ol>

      <div class="container">
        <form name="other_process" id="contactForm" novalidate action="{{ route('contact.other.process')}}" method="post">
        @csrf

          <input type="hidden" name="contact_type" value="{{ $input['contact_type'] }}">
          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">種別<span class="badge badge-danger ml-1">必須</span></p>
            <div class="col-sm-8">
            {{ config('const.CONTACT.USER_TYPE')[$input['user_type']] }}
            </div>
          </div>
          <input type="hidden" name="user_type" value="{{ $input['user_type'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">企業名<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['company_name'] }}
              </div>
          </div>
          <input type="hidden" name="company_name" value="{{ $input['company_name'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">お名前<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['personal_name'] }}
              </div>
          </div>
          <input type="hidden" name="personal_name" value="{{ $input['personal_name'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">電話番号<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['tel'] }}
              </div>
          </div>
          <input type="hidden" name="tel" value="{{ $input['tel'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">メールアドレス<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['email'] }}
              </div>
          </div>
          <input type="hidden" name="email" value="{{ $input['email'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">ご住所</p>
              <div class="col-sm-8">
              {{ $input['address_1'] }}
              {{ $input['address_2'] }}
              {{ $input['address_3'] }}
              </div>
          </div>
          <input type="hidden" name="address_1" value="{{ $input['address_1'] }}">
          <input type="hidden" name="address_2" value="{{ $input['address_2'] }}">
          <input type="hidden" name="address_3" value="{{ $input['address_3'] }}">

          <hr>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">希望連絡時間帯<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['asking_time_zone'] }}
              </div>
          </div>
          <input type="hidden" name="asking_time_zone" value="{{ $input['asking_time_zone'] }}">

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">お問い合わせ内容<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
              {{ $input['comment'] }}
              </div>
          </div>
          <input type="hidden" name="comment" value="{{ $input['comment'] }}">

          <div class="control-group form-group">
            <div class="text-center">
                <button name="action" type="submit" class="btn btn-dark" id="retuenContactButton" value="return">入力画面に戻る</button>
                <button name="action" type="submit" class="btn btn-primary" id="sendContactButton" value="submit">送信</button>
            </div>
          </div>
        </form>
      </div>
    </div>

@include("business.layouts.footer")