<?php

namespace App\Http\Controllers;

use App\Estate;
use Illuminate\Http\Request;
use Carbon\Carbon;

class EstateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Search the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        return view('business.estates.search');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $estateQuery = Estate::query()
        // 地域
        ->when(request('prefecture_id'), function ($query) {
            return $query->whereIn('prefecture_id', request('prefecture_id'));
        })
        // 電圧
        ->when(request('voltage_type'), function ($query) {
            return $query->where('voltage_type', request('voltage_type'));
        })
        // 価格下限
        ->when(request('price_lower'), function ($query) {
            return $query->where('price', '>=', request('price_lower') * 10000);
        })
        // 価格上限
        ->when(request('price_upper'), function ($query) {
            return $query->where('price', '<=', request('price_upper') * 10000);
        })
        // 表面利回り
        ->when(request('coupon_yield'), function ($query) {
            return $query->where('coupon_yield', '>=', request('coupon_yield'));
        })
        // FIT
        ->when(request('fit_unit_price'), function ($query) {
            return $query->where('fit_unit_price', '>=', request('fit_unit_price'));
        })
        // 連系時期開始
        ->when(request('interconnection_date_start'), function ($query) {
            $dt_start = new Carbon(request('interconnection_date_start'));
            return $query->where('interconnection_date', '>=', $dt_start->startOfMonth());
        })
        // 連系時期終了
        ->when(request('interconnection_date_end'), function ($query) {
            $dt_end = new Carbon(request('interconnection_date_end'));
            return $query->where('interconnection_date', '<=', $dt_end->startOfMonth());
        })
        // キーワード
        ->when(request('keyword'), function ($query) {
            return $query->where('keyword', 'like', '%'.request('keyword').'%');
        });

        $paging = [];
        $paging['count'] = $estateQuery->count();
        if(request('page')) {
            $paging['from'] = (request('page') - 1) * 10 + 1;
            $paging['to']   = $paging['count'] >= request('page') * 10 ? request('page') * 10 : $paging['count'];
        } else {
            $paging['from'] = 1;
            $paging['to']   = $paging['count'] >= 10 ? 10 : $paging['count'];
        }

        $estates = $estateQuery->paginate(10);

        $selected_prefecture_name = '';
        foreach (request('prefecture_id') as $prefecture_id) {
            $selected_prefecture_name .= config('const.ESTATE.PREFECTURE')[$prefecture_id] . ', ';
        }

        $params = [
            'prefecture_id'              => request('prefecture_id'),
            'voltage_type'               => request('voltage_type'),
            'price_lower'                => request('price_lower'),
            'price_upper'                => request('price_upper'),
            'coupon_yield'               => request('coupon_yield'),
            'fit_unit_price'             => request('fit_unit_price'),
            'interconnection_date_start' => request('interconnection_date_start'),
            'interconnection_date_end'   => request('interconnection_date_end'),
            'keyword'                    => request('keyword')
        ];

        return view('business.estates.list', compact('estates', 'params', 'selected_prefecture_name', 'paging'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  $estateId
     * @return \Illuminate\Http\Response
     */
    public function show($estateId)
    {
        $estate = Estate::findOrFail($estateId);
        return view('business.estates.detail', compact('estate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Estate  $estate
     * @return \Illuminate\Http\Response
     */
    public function edit(Estate $estate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Estate  $estate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estate $estate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Estate  $estate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estate $estate)
    {
        //
    }
}
