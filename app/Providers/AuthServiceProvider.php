<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // 非会員
        Gate::define('non-member', function ($member) {
            return ($member->member_status == 0);
        });
        // 通常会員
        Gate::define('normal-member', function ($member) {
            return ($member->member_status == 1 && !is_null($member->email_verified_at));
        });
        // 優良会員
        Gate::define('premium-member', function ($member) {
            return ($member->member_status == 2 && !is_null($member->email_verified_at));
        });
    }
}
