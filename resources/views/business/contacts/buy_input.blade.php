@include("business.layouts.common")
@include("business.layouts.navi")

    <div class="container">

      <h1 class="mt-4 mb-3">
        <small>お問い合わせ</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('top.index') }}">TOP</a>
        </li>
        <li class="breadcrumb-item active">問い合わせ</li>
      </ol>

      <div class="container">
        <form name="buy_input" id="contactForm" novalidate action="{{ route('contact.buy.confirm')}}" method="post">
        @csrf

          <input type="hidden" name="contact_type" value="2">

          @if (!empty($estate) || old('to_buy_estate_id') )
          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">物件</p>
              <div class="col-sm-8">
              @if (!empty($estate))
              {{ $estate['estate_name'] }}
              <input type="hidden" name="to_buy_estate_id" value="{{ $estate['id'] }}">
              <input type="hidden" name="estate_name" value="{{ $estate['estate_name'] }}">
              @else
              {{ old('estate_name') }}
              <input type="hidden" name="to_buy_estate_id" value="{{ old('to_buy_estate_id') }}">
              <input type="hidden" name="estate_name" value="{{ old('estate_name') }}">
              @endif
              </div>
          </div>
          <hr>
          @endif

          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">種別<span class="badge badge-danger ml-1">必須</span></p>
            <div class="col-sm-8 radio-btn">
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="user_type_1" name="user_type" class="custom-control-input{{ $errors->has('user_type') ? ' is-invalid':'' }}" value="1" {{ old('user_type')=='1' ? 'checked':'' }} >
                <label class="custom-control-label" for="user_type_1">企業</label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="user_type_2" name="user_type" class="custom-control-input{{ $errors->has('user_type') ? ' is-invalid':'' }}" value="2" {{ old('user_type')=='2' ? 'checked':'' }} >
                <label class="custom-control-label" for="user_type_2">機関投資家</label>
              </div>
              {!! $errors->first('user_type', '<div class="text-danger" role="alert">:message</div>') !!}
            </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">企業名<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                <input type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid':'' }}" id="company_name" name="company_name" value="{{ old('company_name') }}" required>
                {!! $errors->first('company_name', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">お名前<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                <input type="text" class="form-control{{ $errors->has('personal_name') ? ' is-invalid':'' }}" id="personal_name" name="personal_name" value="{{ old('personal_name') }}" required>
                {!! $errors->first('personal_name', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">電話番号<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                <input type="tel" class="form-control{{ $errors->has('tel') ? ' is-invalid':'' }}" id="tel" name="tel" value="{{ old('tel') }}" required>
                {!! $errors->first('tel', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">メールアドレス<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid':'' }}" id="email"" name="email" value="{{ old('email') }}" required>
                {!! $errors->first('email', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">ご住所</p>
              <div class="col-sm-8">
                <input type="text" class="form-control mb-1" id="address_1" name="address_1" value="{{ old('address_1') }}" placeholder="都道府県">
                <input type="text" class="form-control mb-1" id="address_2" name="address_2" value="{{ old('address_2') }}" placeholder="市区町村">
                <input type="text" class="form-control mb-1" id="address_3" name="address_3" value="{{ old('address_3') }}" placeholder="番地・建物名・部屋番号">
              </div>
          </div>

          <hr>

          <div class="control-group form-group row">
            <p class="col-sm-4 col-form-label">お問い合わせの目的</p>
            <div class="col-sm-8">
                <select class="form-control" id="purpose_type" name="purpose_type">
                  <option value="">お選びください</option>
                  @foreach(config('const.CONTACT.PURPOSE_TYPE') as $key => $value)
                  <option value="{{ $key }}" {{ old('purpose_type')==$key ? "selected":"" }} >{{ $value }}</option>
                  @endforeach              
                </select>
              </div>
            </p>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">希望地域</p>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="asking_area" name="asking_area" value="{{ old('asking_area') }}" >
              </div>
          </div>

          <hr>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">希望連絡時間帯<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                <input type="text" class="form-control{{ $errors->has('asking_time_zone') ? ' is-invalid':'' }}" id="asking_time_zone" name="asking_time_zone" value="{{ old('asking_time_zone') }}" required placeholder="例：9時～12時まで">
                {!! $errors->first('asking_time_zone', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group row">
              <p class="col-sm-4 col-form-label">お問い合わせ内容<span class="badge badge-danger ml-1">必須</span></p>
              <div class="col-sm-8">
                  <textarea rows="10" cols="100" class="form-control{{ $errors->has('comment') ? ' is-invalid':'' }}" id="comment" name="comment" required maxlength="999" style="resize:none">{{ old('comment') }}</textarea>
                  {!! $errors->first('comment', '<div class="text-danger" role="alert">:message</div>') !!}
              </div>
          </div>

          <div class="control-group form-group">
            <div class="text-center">
                <button type="submit" class="btn btn-primary" id="confirmContactButton">確認する</button>
            </div>
          </div>
        </form>
      </div>
    </div>

@include("business.layouts.footer")