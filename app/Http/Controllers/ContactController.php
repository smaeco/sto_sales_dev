<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Contact;
use App\Estate;
use App\Mail\ContactMail;
use Config;

class ContactController extends Controller
{
    public function sellInput()
    {
        return view('business.contacts.sell_input');
    }

    public function sellConfirm(Request $request)
    {
        $request->validate([
            'user_type'       => 'required',
            'company_name'     => 'required',
            'personal_name'    => 'required',
            'tel'              => 'required|numeric',
            'email'            => 'required|email',
            'asking_time_zone' => 'required',
            'comment'          => 'required',
        ]);

        $input = $request->all();

        return view('business.contacts.sell_confirm', ['input' => $input]);
    }

    public function sellProcess(Request $request)
    {
        $action = $request->get('action', 'return');
        $input  = $request->except('action');

        if ($action === 'submit') {
            // DBに保存
            $contact = new Contact();
            $contact->fill($input);
            $contact->save();

            // メール送信
            Mail::to($input['email'])->send(new ContactMail('mails.sell_contact', 'お問い合わせありがとうございます', $input));
            Mail::to(Config::get('const.CONTACT_MAIL'))->send(new ContactMail('mails.support_contact', 'お問い合わせがありました', $input));

            return redirect()->route('contact.sell.complete');
        } else {
            return redirect()->route('contact.sell.input')->withInput($input);
        }
    }

    public function sellComplete()
    {
        return view('business.contacts.sell_complete');
    }

    public function buyInput(Request $request)
    {
        $estate = $request->get('estate_id') ? Estate::findOrFail($request->get('estate_id')) : null;
        return view('business.contacts.buy_input', compact('estate'));
    }

    public function buyConfirm(Request $request)
    {
        $request->validate([
            'user_type'       => 'required',
            'company_name'     => 'required',
            'personal_name'    => 'required',
            'tel'              => 'required|numeric',
            'email'            => 'required|email',
            'asking_time_zone' => 'required',
            'comment'          => 'required',
        ]);

        $input = $request->all();

        return view('business.contacts.buy_confirm', ['input' => $input]);
    }

    public function buyProcess(Request $request)
    {
        $action = $request->get('action', 'return');
        $input  = $request->except('action');

        if ($action === 'submit') {
            // DBに保存
            $contact = new Contact();
            $contact->fill($input);
            $contact->save();

            // メール送信
            Mail::to($input['email'])->send(new ContactMail('mails.buy_contact', 'お問い合わせありがとうございます', $input));
            Mail::to(Config::get('const.CONTACT_MAIL'))->send(new ContactMail('mails.support_contact', 'お問い合わせがありました', $input));

            return redirect()->route('contact.buy.complete');
        } else {
            return redirect()->route('contact.buy.input')->withInput($input);
        }
    }

    public function buyComplete()
    {
        return view('business.contacts.buy_complete');
    }

    public function otherInput()
    {
        return view('business.contacts.other_input');
    }

    public function otherConfirm(Request $request)
    {
        $request->validate([
            'user_type'       => 'required',
            'company_name'     => 'required',
            'personal_name'    => 'required',
            'tel'              => 'required|numeric',
            'email'            => 'required|email',
            'asking_time_zone' => 'required',
            'comment'          => 'required',
        ]);

        $input = $request->all();

        return view('business.contacts.other_confirm', ['input' => $input]);
    }

    public function otherProcess(Request $request)
    {
        $action = $request->get('action', 'return');
        $input  = $request->except('action');

        if ($action === 'submit') {
            // DBに保存
            $contact = new Contact();
            $contact->fill($input);
            $contact->save();

            // メール送信
            Mail::to($input['email'])->send(new ContactMail('mails.other_contact', 'お問い合わせありがとうございます', $input));
            Mail::to(Config::get('const.CONTACT_MAIL'))->send(new ContactMail('mails.support_contact', 'お問い合わせがありました', $input));

            return redirect()->route('contact.other.complete');
        } else {
            return redirect()->route('contact.other.input')->withInput($input);
        }
    }

    public function otherComplete()
    {
        return view('business.contacts.other_complete');
    }

}
