<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('member_status')->default(0);
            $table->unsignedTinyInteger('customer_type')->default(0);
            $table->string('company_name', 50);
            $table->string('personal_name', 50);
            $table->string('tel', 20);
            $table->string('email', 20);
            $table->dateTime('email_verified_at')->nullable();
            $table->string('address_1', 20)->nullable();
            $table->string('address_2', 20)->nullable();
            $table->string('address_3', 100)->nullable();
            $table->string('password', 100)->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
