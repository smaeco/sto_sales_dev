@include("business.layouts.common")
@include("business.layouts.navi")

    <div class="container">

      <h1 class="mt-4 mb-3">
        <small>お問い合わせ確認</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('top.index') }}">TOP</a>
        </li>
        <li class="breadcrumb-item active">問い合わせ</li>
      </ol>

      <div class="container">
        <div class="row">
          <h3 class="text-center mt-2 mb-5">お問い合わせありがとうございました。</h3>
          <div class="text-center">
            <a href="{{ route('contact.sell.input') }}" class="btn btn-primary">お問い合わせ入力画面に戻る</a>
          </div>
        </div>
      </div>
    </div>

@include("business.layouts.footer")