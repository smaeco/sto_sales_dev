@include("business.layouts.common")
@include("business.layouts.navi")

  <!-- Page Content -->
  <div class="container">

    <h1 class="my-4">会員専用ページ</h1>

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{ route('top.index') }}">TOP</a>
      </li>
      <li class="breadcrumb-item active">購入・投資</li>
    </ol>

    <img class="img-fluid rounded mb-4" src="http://placehold.it/1200x450?text=Buy Message" alt="">
    <img class="img-fluid rounded mb-4" src="http://placehold.it/1200x200?text=Flow" alt="">

    <div class="text-center">
      <a class="btn btn-success btn-lg m-2" href="{{ route('contact.buy.input') }}">問い合わせ</a>
    </div>

@include("business.estates.recommend")
@include("business.estates.search")

  @can('normal-member')
    <div class="card mb-4">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-6">
            <a href="#">
              <img class="img-fluid rounded" src="http://placehold.it/750x300" alt="">
            </a>
          </div>
          <div class="col-lg-6">
            <h2 class="card-title">優良会員には非公開物件をご紹介</h2>
            <p class="card-text">来社して頂き、秘密保持契約を結んで下さった皆さまは優良会員として、非公開物件をご紹介します</p>
            <a class="btn btn-success mb-2" href="{{ route('contact.other.input') }}">問い合わせ</a>
          </div>
        </div>
      </div>
    </div>
    @endcan

@include("business.layouts.footer")