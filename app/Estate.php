<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estate extends Model
{
    use SoftDeletes;

    protected $table = 'estates';

    protected $dates = ['interconnection_date', 'operation_date', 'deleted_at', 'created_at', 'updated_at'];

    protected $fillable = [
        'estate_name',
        'location',
        'irradiation',
        'area',
        'price',
        'voltage_type',
        'land_contract_type',
        'annual_energy_generation',
        'annual_energy_income',
        'fit_unit_price',
        'interconnection_date',
        'operation_date',
        'coupon_yield',
        'sell_type',
        'output_suppression_flg',
        'auth_status',
        'epc_free_flg',
        'module_maker',
        'module_configure',
        'maximum_output',
        'power_conditioner_maker',
        'power_conditioner_configure',
        'total_system_output',
        'land_price',
        'insurance',
        'monitoring',
        'maintenance',
        'memo_1',
        'memo_2',
        'memo_3',
        'memo_4',
        'memo_5'
    ];
}
