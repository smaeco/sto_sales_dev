@include("admin.layouts.header")
@include("admin.layouts.navi")
@include("admin.layouts.sidebar")

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">会員編集</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}">TOP</a></li>
              <li class="breadcrumb-item active">会員編集</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <form method="POST" action="{{ route('admin.member.update', ['memberId' => $member->id])}}">
        @csrf
        <div class="row">
            <div class="col-md-8">
              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  @if ($errors->any())
                    <div class="callout callout-danger">
                      @foreach (array_unique($errors->all()) as $error)
                      <p class="text-danger" role="alert">※{{ $error }}</p>
                      @endforeach
                    </div>
                  @endif
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <th style="width:200px;">ID</th>
                        <td>{{ $member->id }}</td>
                      </tr>
                      <tr>
                        <th>種別</th>
                        <td>
                          @foreach(config('const.MEMBER.CUSTOMER_TYPE') as $key => $value)
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customer_type{{ $key }}" name="customer_type" class="custom-control-input{{ $errors->has('user_type') ? ' is-invalid':'' }}" value="{{ $key }}" {{ $member->customer_type == $key ? ' checked' : ''}}>
                            <label class="custom-control-label" for="customer_type{{ $key }}">{{ $value }}</label>
                          </div>
                          @endforeach
                        </td>
                      </tr>
                      <tr>
                        <th>会員種別</th>
                        <td>
                          @foreach(config('const.MEMBER.MEMBER_STATUS') as $key => $value)
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="member_status{{ $key }}" name="member_status" class="custom-control-input{{ $errors->has('user_type') ? ' is-invalid':'' }}" value="{{ $key }}" {{ $member->member_status == $key ? ' checked' : ''}}>
                            <label class="custom-control-label" for="member_status{{ $key }}">{{ $value }}</label>
                          </div>
                          @endforeach
                        </td>
                      </tr>
                      <tr>
                        <th>企業名</th>
                        <td>
                          <input type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid':'' }}" name="company_name" value="{{ old('company_name', $member->company_name) }}">
                        </td>
                      </tr>
                      <tr>
                        <th>個人名</th>
                        <td>
                          <input type="text" class="form-control{{ $errors->has('personal_name') ? ' is-invalid':'' }}" name="personal_name" value="{{ old('personal_name', $member->personal_name) }}">
                        </td>
                      </tr>
                      <tr>
                        <th>電話番号</th>
                        <td>
                          <input type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid':'' }}" name="tel" value="{{ old('tel', $member->tel) }}">
                        </td>
                      </tr>
                      <tr>
                        <th>メールアドレス</th>
                        <td>
                          <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid':'' }}" name="email" value="{{ old('email', $member->email) }}">
                        </td>
                      </tr>
                      <tr>
                        <th>住所（都道府県）</th>
                        <td><input type="text" class="form-control" name="address_1" value="{{ old('address_1', $member->address_1) }}"></td>
                      </tr>
                      <tr>
                        <th>住所（市町村）</th>
                        <td><input type="text" class="form-control" name="address_2" value="{{ old('address_2', $member->address_2) }}"></td>
                      </tr>
                      <tr>
                        <th>住所（番地・建物名・部屋番号）</th>
                        <td><input type="text" class="form-control" name="address_3" value="{{ old('address_3', $member->address_3) }}"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <div class="col-md-4">
              <div class="row m-2">
                <a href="{{ route('admin.member.show', ['memberId' => $member->id])}}" type="button" class="btn btn-secondary m-1 w-25 btn-flat">詳細</a>
                <button type="submit" class="btn btn-primary m-1 w-25 btn-flat">更新</button>
              </div>
            </div>
        </div>
        </form>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
  </div>

@include("admin.layouts.footer")
