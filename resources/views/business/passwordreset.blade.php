@include("business.layouts.common")

<div class="form-wrapper">
  <div class="card-body p-4">
    <form class="simple_form login" id="login" action="/signup/confirm" accept-charset="UTF-8" method="post">
      <p>パスワードの再設定メールを送信します。</p>
      <div class="form-group row email required email">
        <label class="col-sm-3 col-form-label email required">メールアドレス</label>
        <div class="col-sm-9">
          <input class="form-control string email required" required="required" aria-required="true" type="email" name="email">
        </div>
      </div>
      <div class="text-center">
        <input type="submit" name="commit" value="パスワード再設定" id="passwordButton" class="btn btn-lg btn-success disabled" data-disable-with="パスワード再設定" style="width: 100%;">
      </div>
    </form>
  </div>
</div>