<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estate;
use Gate;

class TopController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Gate::allows('normal-member') || Gate::allows('premium-member')){
            $recommend_estates = Estate::where('recommend_flg', 1)->get();
            return view('business.top.member_index', compact('recommend_estates'));
        }else{
            return view('business.top.nonmember_index');
        }
    }

    public function sell()
    {
        if(Gate::allows('normal-member') || Gate::allows('premium-member')){
            return view('business.top.member_sell');
        }else{
            return view('business.top.nonmember_sell');
        }
    }
    
    public function buy()
    {
        if(Gate::allows('normal-member') || Gate::allows('premium-member')){
            $recommend_estates = Estate::where('recommend_flg', 1)->get();
            return view('business.top.member_buy', compact('recommend_estates'));
        }else{
            return view('business.top.nonmember_buy');
        }
    }
}
